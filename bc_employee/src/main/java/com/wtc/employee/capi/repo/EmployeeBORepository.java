package com.wtc.employee.capi.repo;

import java.util.List;

import com.intershop.beehive.businessobject.capi.BusinessObjectRepository;
import com.intershop.beehive.foundation.quantity.Money;
import com.wtc.employee.capi.bo.EmployeeBO;

public interface EmployeeBORepository extends BusinessObjectRepository
{
    public static final String EXTENSION_ID = "EmployeeBORepository";

    public EmployeeBO createEmployee(EmployeeBO employeeBO);

    public EmployeeBO registerEmployee(int code, String name, Money salary, String designation);

    public EmployeeBO updateEmployee(String uuid, EmployeeBO employeeBO);

    public void removeEmployee(String uuid);

    public EmployeeBO getEmployee(String uuid);

    public List<EmployeeBO> getAllEmployee();
    
    public void removeEmployeeByCode(Integer employeeID);
    
    public EmployeeBO getEmployeeByEmpCode(Integer empCode);
}
