package com.wtc.employee.capi.businessobjectextension;

import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;

public interface RepositoryBOEmployeeExtension extends BusinessObjectExtension<EmployeeBO>, EmployeeBORepository
{

    public static final String EXTENSION_ID = "RepositoryBOEmployeeExtension";
}
