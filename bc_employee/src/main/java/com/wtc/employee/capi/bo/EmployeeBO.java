package com.wtc.employee.capi.bo;

import com.intershop.beehive.businessobject.capi.BusinessObject;
import com.intershop.beehive.foundation.quantity.Money;

public interface EmployeeBO extends BusinessObject
{

    public int getEmpCode();
    
    public void setEmpCode(int empCode);
    
    public String getEmpName();
    
    public void setEmpName(String empName);
    
    public Money getSalary();
    
    public void setSalary(Money salary);
    
    public String getDesignation();
    
    public void setDesignation(String designation);
}
