package com.wtc.employee.capi.businessobjectextension.impl;

import com.intershop.beehive.businessobject.capi.AbstractBusinessObjectExtension;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.businessobjectextension.EmployeeBOAppliedHelloExtension;

public class EmployeeBOAppliedHelloExtensionImpl extends AbstractBusinessObjectExtension<EmployeeBO> implements EmployeeBOAppliedHelloExtension
{
    public EmployeeBOAppliedHelloExtensionImpl(String extensionID, EmployeeBO extendedObject)
    {
        super(extensionID, extendedObject);
    }

    @Override
    public String getHelloName()
    {
        // TODO Auto-generated method stub
        return "Hello "+ getExtendedObject().getEmpName();
    }
}
