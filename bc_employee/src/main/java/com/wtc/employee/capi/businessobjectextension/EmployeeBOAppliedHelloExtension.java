package com.wtc.employee.capi.businessobjectextension;

import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.wtc.employee.capi.bo.EmployeeBO;

public interface EmployeeBOAppliedHelloExtension extends BusinessObjectExtension<EmployeeBO>
{
    public static final String EXTENSION_ID = "AppliedHello";
    
    public String getHelloName();
}
