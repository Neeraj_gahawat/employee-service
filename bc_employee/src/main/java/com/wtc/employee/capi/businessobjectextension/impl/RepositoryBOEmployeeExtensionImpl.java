package com.wtc.employee.capi.businessobjectextension.impl;

import com.intershop.beehive.businessobject.capi.AbstractBusinessObjectExtensionFactory;
import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.businessobjectextension.EmployeeBOAppliedHelloExtension;

public class RepositoryBOEmployeeExtensionImpl extends AbstractBusinessObjectExtensionFactory<EmployeeBO>
{

    @Override
    public BusinessObjectExtension<EmployeeBO> createExtension(EmployeeBO object)
    {
        // TODO Auto-generated method stub
        return new EmployeeBOAppliedHelloExtensionImpl(EmployeeBOAppliedHelloExtension.EXTENSION_ID, object);
    }
    
    @Override
    public Class<EmployeeBO> getExtendedType()
    {
        return EmployeeBO.class;
    }   
 
    @Override
    public Class<EmployeeBOAppliedHelloExtension> getExtensionType()
    {
        return EmployeeBOAppliedHelloExtension.class;
    }

}
