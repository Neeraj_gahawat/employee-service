package com.wtc.employee.pipelet;

import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;

/**
 * Get Employee BO
 */
public class GetEmployeeBO extends Pipelet
{
    public static final String IN_UUID = "edit_uuid";

    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        String uuid = aPipelineDictionary.getRequired(IN_UUID);

        EmployeeBORepository employeeBORepository = aPipelineDictionary.getRequired("EmployeeBORepository");

        EmployeeBO employeeBO = employeeBORepository.getEmployee(uuid);

        if (employeeBO == null)
        {
            Logger.error(this, "employee '{}' not found in DB");
            return PIPELET_ERROR;
        }
        
        Logger.debug(this, "Get Employee BO uuid {}."+uuid);
        aPipelineDictionary.put("employeeBO", employeeBO);

        return PIPELET_NEXT;
    }

}
