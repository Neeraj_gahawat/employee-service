package com.wtc.employee.pipelet;

import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.foundation.util.ResettableIteratorImpl;
import com.wtc.employee.capi.repo.EmployeeBORepository;

/**
 * Remove Employee
 */
public class RemoveEmployeeBO extends Pipelet
{

    public static final String IN_UUID = "uuids";
    
    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        ResettableIteratorImpl<String> uuidList = aPipelineDictionary.getRequired(IN_UUID);

        EmployeeBORepository employeeBORepository = aPipelineDictionary.getRequired("EmployeeBORepository");

        uuidList.forEachRemaining(u->{
            employeeBORepository.removeEmployee(u);
            Logger.info(this, " Employee removed uuid {}."+u);
        });
        
       aPipelineDictionary.put("removeEmployeeMsg", "Employee removed successfully!");
        
        return PIPELET_NEXT;
    }

}
