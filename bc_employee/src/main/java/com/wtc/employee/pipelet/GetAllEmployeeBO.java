package com.wtc.employee.pipelet;

import java.util.List;

import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;

/**
 * Get All Employee BO
 */
public class GetAllEmployeeBO extends Pipelet
{

    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        Logger.debug(this, "Get All Employee BO.");
        
        EmployeeBORepository employeeBORepository = aPipelineDictionary.getRequired("EmployeeBORepository");

        List<EmployeeBO> employeeBOs = employeeBORepository.getAllEmployee();

        aPipelineDictionary.put("employeeBOs", employeeBOs);

        return PIPELET_NEXT;
    }

}
