/**
 * 
 */
package com.wtc.employee.pipelet;

import java.math.BigDecimal;

import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.foundation.quantity.Money;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;

/**
 * Register Employee
 */
public class RegisterEmployee extends Pipelet
{
    public static final String IN_UUID = "uuid";
    public static final String IN_EMP_CODE = "Code";
    public static final String IN_EMP_NAME = "Name";
    public static final String IN_EMP_SALARY = "Salary";
    public static final String IN_EMP_DESIGNATION = "Designation";

    public static final String EMPLOYEE_REPOSITORY = "EmployeeBORepository";

    EmployeeBO employeeBO = null;
    BigDecimal salary = null;

    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        String uuid = aPipelineDictionary.getOptional(IN_UUID);

        int code = Integer.parseInt(aPipelineDictionary.getRequired(IN_EMP_CODE));

        String name = aPipelineDictionary.getRequired(IN_EMP_NAME);

        String strSalary = aPipelineDictionary.getOptional(IN_EMP_SALARY);

        salary = (strSalary != null && !strSalary.isEmpty()) ? new BigDecimal(strSalary) : BigDecimal.ZERO;

        String designation = aPipelineDictionary.getOptional(IN_EMP_DESIGNATION);

        EmployeeBORepository employeeBORepository = aPipelineDictionary.getRequired(EMPLOYEE_REPOSITORY);

        if (uuid == null)
        {
            Logger.debug(this, "New Employee created '{}' code."+code);
            employeeBO = employeeBORepository.registerEmployee(code, name, new Money(Request.getCurrent()
                            .getCurrencyCode(), salary), designation);
            aPipelineDictionary.put("newEmployeeMsg", "Employee registred successfully!");
        }
        else
        {
            Logger.debug(this, "Update Employee '{}' uuid."+uuid);
            employeeBO = employeeBORepository.getEmployee(uuid);

            employeeBO.setEmpCode(code);
            employeeBO.setEmpName(name);
            employeeBO.setDesignation(designation);
            employeeBO.setSalary(new Money(Request.getCurrent()
                            .getCurrencyCode(), salary));

            employeeBO = employeeBORepository.updateEmployee(uuid, employeeBO);
            aPipelineDictionary.put("updateEmployeeMsg", "Employee updated successfully!");
        }

        if (employeeBO == null)
        {
            Logger.error(this, "employee '{}' not found in DB");
            return PIPELET_ERROR;
        }
        
        
        return PIPELET_NEXT;
    }

}
