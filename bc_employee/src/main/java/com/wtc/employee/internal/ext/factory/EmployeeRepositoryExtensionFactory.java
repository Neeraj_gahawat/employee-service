package com.wtc.employee.internal.ext.factory;

import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtensionFactory;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;
import com.wtc.employee.internal.repo.EmployeeBORepositoryImpl;

public class EmployeeRepositoryExtensionFactory extends AbstractDomainRepositoryBOExtensionFactory
{

    @Override
    public BusinessObjectExtension<RepositoryBO> createExtension(RepositoryBO object)
    {
        // TODO Auto-generated method stub
        return new EmployeeBORepositoryImpl(EmployeeBORepository.EXTENSION_ID, object);
    }

    @Override
    public String getExtensionID()
    {
        // TODO Auto-generated method stub
        return EmployeeBORepository.EXTENSION_ID;
    }

}
