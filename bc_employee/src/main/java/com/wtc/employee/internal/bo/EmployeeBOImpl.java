package com.wtc.employee.internal.bo;

import com.intershop.beehive.businessobject.capi.BusinessObjectContext;
import com.intershop.beehive.core.capi.domain.AbstractPersistentObjectBO;
import com.intershop.beehive.foundation.quantity.Money;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.internal.EmployeePO;

public class EmployeeBOImpl extends AbstractPersistentObjectBO<EmployeePO> implements EmployeeBO
{

    public EmployeeBOImpl(EmployeePO delegate, BusinessObjectContext context)
    {
        super(delegate, context);
    }
    
    @Override
    public int getEmpCode()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getEmpCode();
    }

    @Override
    public void setEmpCode(int empCode)
    {
        getPersistentObject().setEmpCode(empCode);
        
    }

    @Override
    public String getEmpName()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getName();
    }

    @Override
    public void setEmpName(String empName)
    {
        // TODO Auto-generated method stub
        getPersistentObject().setName(empName);
    }

    @Override
    public Money getSalary()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getSalary();
    }

    @Override
    public void setSalary(Money salary)
    {
        // TODO Auto-generated method stub
        getPersistentObject().setSalary(salary);
    }

    @Override
    public String getDesignation()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getDesignation();
    }

    @Override
    public void setDesignation(String designation)
    {
        // TODO Auto-generated method stub
        getPersistentObject().setDesignation(designation);
    }
    
}
