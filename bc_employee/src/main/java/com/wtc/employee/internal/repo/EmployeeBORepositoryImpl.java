package com.wtc.employee.internal.repo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.inject.Inject;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.foundation.quantity.Money;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtension;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;
import com.wtc.employee.internal.EmployeePO;
import com.wtc.employee.internal.EmployeePOFactory;
import com.wtc.employee.internal.EmployeePOKey;
import com.wtc.employee.internal.bo.EmployeeBOImpl;

public class EmployeeBORepositoryImpl extends AbstractDomainRepositoryBOExtension implements EmployeeBORepository
{

    @Inject
    private EmployeePOFactory employeePOFactory;

    public EmployeeBORepositoryImpl(String extensionID, RepositoryBO extendedObject)
    {
        super(extensionID, extendedObject);
        NamingMgr.injectMembers(this);
    }

    @Override
    public EmployeeBO createEmployee(EmployeeBO employeeBO)
    {
        EmployeePO employeePO = employeePOFactory.create(getDomain(), employeeBO.getEmpCode(), employeeBO.getEmpName());
        employeePO.setSalary(employeeBO.getSalary());
        employeePO.setDesignation(employeeBO.getDesignation());

        return new EmployeeBOImpl(employeePO, getContext());

    }

    @Override
    public EmployeeBO registerEmployee(int code, String name, Money salary, String designation)
    {
        EmployeePO employeePO = employeePOFactory.create(getDomain(), code, name);
        employeePO.setSalary(salary);
        employeePO.setDesignation(designation);

        return new EmployeeBOImpl(employeePO, getContext());
    }

    @Override
    public EmployeeBO updateEmployee(String uuid, EmployeeBO employeeBO)
    {
        EmployeePO employeePO = employeePOFactory.getObjectByPrimaryKey(new EmployeePOKey(uuid));
        employeePO.setName(employeeBO.getEmpName());
        employeePO.setSalary(employeeBO.getSalary());
        employeePO.setDesignation(employeeBO.getDesignation());
        return new EmployeeBOImpl(employeePO, getContext());
    }

    @Override
    public void removeEmployee(String uuid)
    {
        employeePOFactory.remove(new EmployeePOKey(uuid));
    }

    @Override
    public EmployeeBO getEmployee(String uuid)
    {
        return new EmployeeBOImpl(employeePOFactory.getObjectByPrimaryKey(new EmployeePOKey(uuid)), getContext());
    }

    @Override
    public List<EmployeeBO> getAllEmployee()
    {
        List<EmployeeBO> employees = new ArrayList<>();
        List<EmployeePO> list = (List<EmployeePO>)employeePOFactory.getAllObjects()
                        .stream()
                        .collect(Collectors.toList());

        for (EmployeePO employeePO : list)
        {
            employees.add(new EmployeeBOImpl(employeePO, getContext()));
        }
        return employees;
    }

    @Override
    public void removeEmployeeByCode(Integer empCode)
    {
        @SuppressWarnings("unchecked")
        Collection<EmployeePO> collection = employeePOFactory.getObjectsBySQLWhere("EMPCODE=?",
                        new Integer[] { empCode });

        if (!collection.isEmpty())
        {
            removeEmployee(collection.stream()
                            .findFirst()
                            .get()
                            .getUUID());
        }
    }

    @Override
    public EmployeeBO getEmployeeByEmpCode(Integer empCode)
    {
        EmployeePO employeePO = null;
        @SuppressWarnings("unchecked")
        Collection<EmployeePO> collection = employeePOFactory.getObjectsBySQLWhere("EMPCODE=?",
                        new Integer[] { empCode });
        if (!collection.isEmpty())
        {
            employeePO = collection.stream()
                            .findFirst()
                            .get();
        }
        return new EmployeeBOImpl(employeePO, getContext());
    }
}
