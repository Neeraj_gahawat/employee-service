// =============================================================================
// File: EmployeePO.java
// Generated by JGen Code Generator from INTERSHOP Communications AG.
// Generator template: ORMClass.xpt(checksum: 9a846fb10e9c3b98b1a62ea7a508da27)
// =============================================================================
// The JGen Code Generator software is the property of INTERSHOP Communications AG. 
// Any rights to use are granted under the license agreement. 
// =============================================================================
package com.wtc.employee.internal;

import com.intershop.beehive.core.capi.domain.AttributeValue;
import com.intershop.beehive.core.capi.domain.ExtensibleObjectPO;
import com.intershop.beehive.foundation.quantity.Money;
import com.intershop.beehive.orm.capi.common.ORMObjectFactory;
import com.intershop.beehive.orm.capi.description.AttributeDescription;
import com.intershop.beehive.orm.capi.description.RelationDescription;
import com.wtc.employee.capi.Employee;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author ngahlawat
 * @generated
 */
public class EmployeePO extends ExtensibleObjectPO implements Employee
{

    /**
     * @generated
     */
    public static AttributeDescription empCodeAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription nameAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription designationAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription salaryCodeAttribute = null;

    /**
     * @generated
     */
    public static AttributeDescription salaryValueAttribute = null;

    /**
     * @generated
     */
    public static RelationDescription attributeValuePOsRelation = null;

    /**
     * The constructor.
     *
     * @generated
     */
    public EmployeePO(ORMObjectFactory factory, EmployeePOKey key)
    {
        super(factory, key);
    }

    /**
     * Returns the name of the factory that manages this type of objects. The name can be used to lookup the factory
     * from the NamingMgr.
     *
     * @return the factory name
     * @generated
     */
    public String getFactoryName()
    {
        return getClass().getName();
    }

    /**
     * Loads the state of the object from the database.
     *
     * @deprecated use refresh now
     * @generated
     */
    public void load()
    {
        refresh();
    }

    /**
     * This hook is called whenever the bean has been modified.
     *
     * @generated modifiable
     */
    public void onChange()
    {
        // {{ bean_onchange
        // put your custom onChange code here
        // }} bean_onchange

        super.onChange();
    }

    /**
     * Returns the value of attribute 'empCode'.
     * <p>
     *
     * @return the value of the attribute 'empCode'
     * @generated
     */
    public int getEmpCode()
    {
        Integer value = (Integer)getAttributeValue(empCodeAttribute);
        return (value != null) ? value.intValue() : (int)0;
    }

    /**
     * Sets the value of the attribute 'empCode'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setEmpCode(int aValue)
    {

        if (setAttributeValue(empCodeAttribute, new Integer(aValue)))
        {
            onChange();
        }
    }

    /**
     * Returns the value of attribute 'name'.
     * <p>
     *
     * @return the value of the attribute 'name'
     * @generated
     */
    public String getName()
    {
        String value = (String)getAttributeValue(nameAttribute);
        return (value != null) ? value : "";
    }

    /**
     * Sets the value of the attribute 'name'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setName(String aValue)
    {

        if (setAttributeValue(nameAttribute, aValue))
        {
            onChange();
        }
    }

    /**
     * Returns the value of attribute 'designation'.
     * <p>
     *
     * @return the value of the attribute 'designation'
     * @generated
     */
    public String getDesignation()
    {
        String value = (String)getAttributeValue(designationAttribute);
        return (value != null) ? value : "";
    }

    /**
     * Sets the value of the attribute 'designation'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setDesignation(String aValue)
    {

        if (setAttributeValue(designationAttribute, aValue))
        {
            onChange();
        }
    }

    /**
     * Checks whether the value of the attribute 'designation' is null.
     * <p>
     *
     * @return true if the value of attribute 'designation' is null, false otherwise
     * @generated
     */
    public boolean getDesignationNull()
    {
        return (getAttributeValue(designationAttribute) == null);
    }

    /**
     * Sets the value of the attribute 'designation' to null.
     * <p>
     *
     * @param aFlag meaningless
     * @generated
     */
    public void setDesignationNull(boolean aFlag)
    {

        if (setAttributeValue(designationAttribute, null))
        {

            onChange();
        }
    }

    /**
     * Returns the value of attribute 'salary'.
     * <p>
     *
     * @return the value of the attribute 'salary'
     * @generated
     */
    public Money getSalary()
    {
        String code = (String)getAttributeValue(salaryCodeAttribute);
        BigDecimal value = (BigDecimal)getAttributeValue(salaryValueAttribute);
        return new Money((code != null) ? code : "", (value != null) ? value : new BigDecimal("0"));
    }

    /**
     * Sets the value of the attribute 'salary'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setSalary(Money aValue)
    {

        if (setAttributeValue(salaryCodeAttribute, aValue.getCurrencyMnemonic())
                        | setAttributeValue(salaryValueAttribute, aValue.getValue()))
        {
            onChange();
        }
    }

    /**
     * Checks whether the value of the attribute 'salary' is null.
     * <p>
     *
     * @return true if the value of attribute 'salary' is null, false otherwise
     * @generated
     */
    public boolean getSalaryNull()
    {
        return (getAttributeValue(salaryCodeAttribute) == null) || (getAttributeValue(salaryValueAttribute) == null);
    }

    /**
     * Sets the value of the attribute 'salary' to null.
     * <p>
     *
     * @param aFlag meaningless
     * @generated
     */
    public void setSalaryNull(boolean aFlag)
    {

        if (setAttributeValue(salaryCodeAttribute, null) | setAttributeValue(salaryValueAttribute, null))
        {

            onChange();
        }
    }

    /**
     * Returns all associated objects via the relation 'attributeValuePOs'.
     *
     * @return the enumeration of associated objects
     * @generated
     */
    @SuppressWarnings("unchecked")
    public Collection<EmployeePOAttributeValuePO> getAttributeValuePOs()
    {
        return getRelatedObjects(attributeValuePOsRelation);
    }

    /**
     * Checks whether the specified element participates in the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @param anElement the element to check for participation
     * @return true, if the element is part of the relationship, false otherwise
     * @generated
     */
    public boolean isInAttributeValuePOs(EmployeePOAttributeValuePO anElement)
    {
        return getAttributeValuePOs().contains(anElement);
    }

    /**
     * Determines the number of elements participating in the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @return the number of elements participating in the relation
     * @generated
     */
    public int getAttributeValuePOsCount()
    {
        return getAttributeValuePOs().size();
    }

    /**
     * Creates an iterator containing the elements of the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @return the iterator with the elements of the relation
     * @generated
     */
    public Iterator<EmployeePOAttributeValuePO> createAttributeValuePOsIterator()
    {
        return getAttributeValuePOs().iterator();
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @param anElement the element to check for participation
     * @return true, if the element is part of the relationship, false otherwise
     * @generated
     */
    public boolean isInAttributeValues(AttributeValue anElement)
    {
        return isInAttributeValuePOs((EmployeePOAttributeValuePO)anElement);
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @return the number of elements participating in the relation
     * @generated
     */
    public int getAttributeValuesCount()
    {
        return getAttributeValuePOsCount();
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @return the iterator with the elements of the relation
     * @generated
     */
    @SuppressWarnings("unchecked")
    public Iterator<AttributeValue> createAttributeValuesIterator()
    {
        return (Iterator)createAttributeValuePOsIterator();
    }

}
