package com.wtc.employee.internal.employee;

import java.util.Objects;

import javax.inject.Inject;

import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContext;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;
import com.wtc.employee.internal.EmployeePO;

public class EmployeePOToEmployeeBOMapperImpl implements ObjectMapper<EmployeePO, EmployeeBO>
{
    
    @Inject
    BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;

    @Override
    public EmployeeBO resolve(EmployeePO employeePO)
    {
        Objects.requireNonNull(employeePO);

        BusinessObjectRepositoryContext businessObjectRepositoryContext = businessObjectRepositoryContextProvider
                        .getBusinessObjectRepositoryContext();
        EmployeeBORepository employeeBORepository = businessObjectRepositoryContext
                        .getRepository(EmployeeBORepository.EXTENSION_ID);

        return employeeBORepository.getEmployee(employeePO.getUUID());
    }

}
