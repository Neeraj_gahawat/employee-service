package com.wtc.rest.capi.resourceobject;

import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;

public class MessageRO extends AbstractResourceObject
{

    private String message;

    private String code;

    private String description;

    private String status;

    public MessageRO()
    {

    }

    public MessageRO(String message, String code, String description, String status)
    {
        super();
        this.message = message;
        this.code = code;
        this.description = description;
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

}
