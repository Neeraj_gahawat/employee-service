package com.wtc.rest.capi;

import java.util.Iterator;
import java.util.List;

import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.internal.EmployeePO;
import com.wtc.rest.capi.resourceobject.EmployeeRO;

public interface EmployeeResourceHandler
{
    public List<EmployeeBO> getAll();
    
    public EmployeeBO getEmployee(Integer empCode);

    public EmployeeBO addEmployee(EmployeeRO employeeRO);

    public boolean deleteEmployee(Integer empCode);

    public EmployeeBO updateEmployee(EmployeeRO employeeRO);

    public Iterator<EmployeePO> getEmployeeListBySimpleSearch(Integer pageSize, Integer pageNumber);

    public List<EmployeeBO> getEmployeeBySalaryRange(Integer minSal, Integer maxSal);

    
    
}
