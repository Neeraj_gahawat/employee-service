package com.wtc.rest.capi.resourceobject;

import com.intershop.beehive.foundation.quantity.Money;
import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Employee RO")
public class EmployeeRO extends AbstractResourceObject
{
    private Integer empCode;

    private String empName;

    private Money salary;

    private String desigantion;
    
    public EmployeeRO() {
        
    }

    public EmployeeRO(Integer empCode, String empName, Money salary, String desigantion)
    {
        super();
        this.empCode = empCode;
        this.empName = empName;
        this.salary = salary;
        this.desigantion = desigantion;
    }

    @ApiModelProperty(value = "EmployeeCode", example = "1224")
    public Integer getEmpCode()
    {
        return empCode;
    }

    public void setEmpCode(Integer empCode)
    {
        this.empCode = empCode;
    }

    @ApiModelProperty(value = "EmployeeName", example = "Test")
    public String getEmpName()
    {
        return empName;
    }

    public void setEmpName(String empName)
    {
        this.empName = empName;
    }
    
    @ApiModelProperty(value = "Salary", example = "10000")
    public Money getSalary()
    {
        return salary;
    }

    public void setSalary(Money salary)
    {
        this.salary = salary;
    }
    
    @ApiModelProperty(value = "Desigantion", example = "Developer")
    public String getDesigantion()
    {
        return desigantion;
    }

    public void setDesigantion(String desigantion)
    {
        this.desigantion = desigantion;
    }

}
