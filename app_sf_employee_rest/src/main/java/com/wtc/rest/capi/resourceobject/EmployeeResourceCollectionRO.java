package com.wtc.rest.capi.resourceobject;

import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;
import com.intershop.component.rest.capi.resourceobject.ResourceCollectionRO;

public class EmployeeResourceCollectionRO extends ResourceCollectionRO<AbstractResourceObject>
{

    public String info = null;

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }
}
