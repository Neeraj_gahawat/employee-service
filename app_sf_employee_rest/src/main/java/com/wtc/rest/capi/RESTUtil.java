package com.wtc.rest.capi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import com.intershop.beehive.app.capi.AppContext;
import com.intershop.beehive.businessobject.capi.BusinessObjectContext;
import com.intershop.beehive.core.capi.app.AppContextUtil;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.paging.PageableIterator;
import com.intershop.beehive.core.capi.paging.PagingMgr;
import com.intershop.component.rest.capi.paging.PagingUtils;
import com.intershop.component.rest.capi.resourceobject.ResourceCollectionRO;

public abstract class RESTUtil
{
    private RESTUtil()
    {
    }

    /**
     * Return the business context for the current application bo.
     * 
     * @return business context
     */
    public static BusinessObjectContext getContext()
    {
        AppContext appContext = AppContextUtil.getCurrentAppContext();

        BusinessObjectContext boContext;
        boContext = appContext.getVariable(BusinessObjectContext.NAME);

        return boContext;
    }

    /**
     * calculate from offset and amount minimal needed page size
     * 
     * @param offset
     * @param amount
     * @return
     */
    public static Integer calcPageSize(Integer offset, Integer amount)
    {
        double upper;
        double lower;

        Integer pageableAmount = (amount == 1) ? (amount + 1) : amount;
        Integer pageableOffset = offset;

        if (pageableOffset == 0)
        {
            return pageableAmount;
        }
        if (pageableAmount == 0)
        {
            return pageableAmount;
        }

        int t = pageableOffset + pageableAmount;
        int kStart = (t / pageableAmount);
        for (int k = kStart; k >= 0; k--)
        {
            lower = Math.ceil((double)t / (double)(k + 1));
            upper = Math.floor((double)pageableOffset / (double)k);
            if (lower <= upper)
            {
                return (int)Math.round(lower);
            }
        }
        return pageableAmount;
    }

    /**
     * calculate from offset and amount page number for minimal page size
     * 
     * @param offset
     * @param amount
     * @return
     */
    public static Integer calcPageNumber(Integer offset, Integer amount)
    {
        if (amount == 0)
        {
            return amount;
        }

        Integer pageSize = calcPageSize(offset, amount);
        if (pageSize == 0)
        {
            throw new ArithmeticException("/ by zero");
        }
        return (offset / pageSize);

    }

    /**
     * prepares a ResourceCollection in terms of paging & sorting
     * 
     * used by OrderList and ProductList
     * 
     * @param collectionRO
     * @param objects
     * @param returnSortKeys
     * @param offset
     * @param amount
     * @param total
     * @param sortableAttributes
     * @param defaultPageSize
     * @return
     *//*
        * public static <T> Iterator<T> prepareCollection(ResourceCollectionRO collectionRO, Iterator<T> objects,
        * Boolean returnSortKeys, Integer offset, Integer amount, Integer total, Iterator<SortableAttribute>
        * sortableAttributes, Integer defaultPageSize) { HashSet<String> sortKeys = new HashSet<>(); Integer
        * pageableAmount = (amount == null) ? defaultPageSize : amount; Integer pageableOffset = (offset == null) ? 0 :
        * offset; updateSortKeys(collectionRO, sortKeys, returnSortKeys, sortableAttributes); if (objects instanceof
        * PageableIterator) { if (pageableAmount == -1) { pageableAmount =
        * ((PageableIterator<T>)objects).getElementCount() - pageableOffset; } if
        * (((PageableIterator<T>)objects).getElementCount() > pageableAmount) { String pageableID =
        * ((PageableIterator<T>)objects).getID(); PagingMgr pagingMgr =
        * (PagingMgr)NamingMgr.getInstance().lookupManager(PagingMgr.REGISTRY_NAME);
        * pagingMgr.registerPageable(pageableID, (PageableIterator<T>)objects); collectionRO.setPageable(pageableID);
        * collectionRO.setOffset(pageableOffset); collectionRO.setAmount(pageableAmount); }
        * collectionRO.setTotal(total); return
        * (Iterator<T>)PagingUtils.getRangeFromPageable(((PageableIterator<T>)objects), pageableOffset, pageableAmount);
        * } // search engine based product lists else { // having no pageable means: the resulting iterator includes
        * exactly the requested items if (total > pageableAmount) { collectionRO.setOffset(pageableOffset);
        * collectionRO.setAmount(pageableAmount); collectionRO.setTotal(total); } Integer pageSize =
        * calcPageSize(pageableOffset, pageableAmount); Integer pageNumber = calcPageNumber(pageableOffset,
        * pageableAmount); Integer inPageOffset = pageableOffset - pageSize * pageNumber; Collection<T> resultBOs = new
        * ArrayList<>(); ((ResettableIterator<T>)objects).reset(); // remove items at the beginning of the page for (int
        * n = 0; n < inPageOffset; n++) { objects.next(); } // skip amount items for (int n = 0; n < pageableAmount &&
        * objects.hasNext(); n++) { resultBOs.add(objects.next()); } return resultBOs.iterator(); } }
        */

}