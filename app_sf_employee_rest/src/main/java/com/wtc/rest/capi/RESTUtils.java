package com.wtc.rest.capi;

import com.intershop.beehive.app.capi.AppContext;
import com.intershop.beehive.businessobject.capi.BusinessObjectContext;
import com.intershop.beehive.core.capi.app.AppContextUtil;

public abstract class RESTUtils
{
    private RESTUtils()
    {
    }

    /**
     * Return the business context for the current application bo.
     * 
     * @return business context
     */
    public static BusinessObjectContext getContext()
    {
        AppContext appContext = AppContextUtil.getCurrentAppContext();

        BusinessObjectContext boContext;
        boContext = appContext.getVariable(BusinessObjectContext.NAME);

        return boContext;
    }
}