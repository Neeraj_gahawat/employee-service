package com.wtc.rest.capi.resourceobject;

import java.util.List;

public class ResourceCollectionMessageROList extends EmployeeResourceCollectionRO
{

    private List<MessageRO> result;

    public List<MessageRO> getResult()
    {
        return result;
    }

    public void setResult(List<MessageRO> result)
    {
        this.result = result;
    }
}
