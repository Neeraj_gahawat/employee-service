package com.wtc.rest.capi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.paging.PagingMgr;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.core.capi.request.Session;
import com.intershop.beehive.core.capi.user.User;
import com.intershop.component.application.capi.CurrentApplicationBOProvider;
import com.intershop.component.rest.capi.RestException;
import com.intershop.component.rest.capi.resource.AbstractRestResource;
import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;
import com.intershop.component.rest.capi.resourceobject.ResourceCollectionRO;
import com.intershop.component.rest.capi.response.ResponseStatusConstants;
import com.intershop.component.user.capi.UserBO;
import com.intershop.component.user.capi.UserBORepository;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.internal.EmployeePO;
import com.wtc.rest.capi.resourceobject.EmployeeRO;
import com.wtc.rest.capi.resourceobject.EmployeeResourceCollectionRO;
import com.wtc.rest.capi.resourceobject.MessageRO;
import com.wtc.rest.capi.resourceobject.MessageROList;
import com.wtc.rest.capi.resourceobject.ResourceCollectionMessageROList;
import com.wtc.rest.internal.EmployeeBOToEmployeeROMapperImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition(basePath = "Employee", info = @Info(version = "1.0", title = "Employee Rest CURD API",
                description = "Employee REST API allows to retrieve, store, update and delete information about employees."))
@Api("Employee API")
public class EmployeeResource extends AbstractRestResource
{
    public static final String EMPLOYEE_CODE = "empCode";
    public final static String VERSION2 = "application/vnd.intershop.V2+";

    public final static String VER2 = "application/V2+";
    public final static String JSON_SUFFIX = "json";

    private static final String EMPLOYEES = "employees";
    private EmployeeResourceHandler handler;

    @Inject
    private CurrentApplicationBOProvider applicationBOProvider;

    @Inject
    private EmployeeBOToEmployeeROMapperImpl employeeROCollectionMapper;

    public EmployeeResourceHandler getHandler()
    {
        return handler;
    }

    public void setHandler(EmployeeResourceHandler handler)
    {
        this.handler = handler;
    }

    @Override
    public EmployeeResource getRequestSpecificCopy(ResourceContext rc)
    {

        EmployeeResource result = (EmployeeResource)super.getRequestSpecificCopy(rc);
        result.setHandler(this.handler);
        return result;
    }

    @ApiOperation(value = "Get All Employee", response = EmployeeRO.class)
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = ResponseStatusConstants.OK_MESSAGE),
                    @ApiResponse(code = ResponseStatusConstants.NOT_FOUND,
                                    message = ResponseStatusConstants.NOT_FOUND_MESSAGE) })

    @GET
    @Produces("application/json")
    public EmployeeResourceCollectionRO getAllEmployee()
    {
        setCacheExpires(0);
        EmployeeResourceCollectionRO collectionRO = new EmployeeResourceCollectionRO();
        List<MessageRO> list = new ArrayList<>();
        ResourceCollectionMessageROList messageROList = new ResourceCollectionMessageROList();
        Logger.debug(this, " EmployeeResource getAllEmployee");

        try
        {
            if (isCurrentUserLoggedIn())
            {
                List<EmployeeBO> employeeBOList = handler.getAll();
                if (employeeBOList != null)
                {
                    employeeBOList.stream()
                                    .forEach(e -> {
                                        collectionRO.addElement(employeeROCollectionMapper.resolve(e));

                                    });
                    int total = (int)employeeBOList.stream()
                                    .count();
                    collectionRO.setTotal(total);
                    collectionRO.setInfo("" + total + " list items");
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                }
                else
                {
                    addResponseData(HttpServletResponse.SC_NOT_FOUND, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                    return messageROList;
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            addResponseData(ResponseStatusConstants.INTERNAL_SERVER_ERROR, ErrorFlowConstants.EMPLOYEE_NOT_GET,
                            getName());
            list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.EMPLOYEE_NOT_FOUND,
                            ErrorFlowConstants.ERROR_CODE_EM002, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource getAllEmployee()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND, e);

            return collectionRO;
        }

        return collectionRO;
    }

    @ApiOperation("Get Employee")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Get Employee",
                    response = EmployeeRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Employee could not be get.", response = RestException.class) })
    @GET
    @Path("code/{" + EMPLOYEE_CODE + ":[^/]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public AbstractResourceObject getEmployee(@PathParam(EMPLOYEE_CODE) Integer empCode)
    {
        List<MessageRO> list = new ArrayList<>();
        MessageROList messageROList = new MessageROList();

        setCacheExpires(0);
        Logger.debug(this, " EmployeeResource getEmployee");

        try
        {
            if (isCurrentUserLoggedIn())
            {
                EmployeeBO employeeBO = handler.getEmployee(empCode);
                if (employeeBO != null)
                {
                    addResponseData(HttpServletResponse.SC_OK, "getEmployee", getName());
                    return employeeROCollectionMapper.resolve(employeeBO);
                }
                else
                {
                    addResponseData(HttpServletResponse.SC_OK, "getEmployee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "getEmployee", getName());
            list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                            ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource getEmployee()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND, e);
        }
        return messageROList;
    }

    @ApiOperation("Add Employee")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.CREATED, message = "Add Employee",
                    response = EmployeeRO.class) })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AbstractResourceObject addEmployee(EmployeeRO employeeRO)
    {
        Logger.debug(this, " EmployeeResource addEmployee");
        List<MessageRO> list = new ArrayList<>();
        MessageROList messageROList = new MessageROList();
        setCacheExpires(0L);
        try
        {
            if (isCurrentUserLoggedIn())
            {
                EmployeeBO employeeBO = handler.getEmployee(employeeRO.getEmpCode());
                if (employeeBO != null && employeeBO.getID() != null && employeeBO.getEmpCode() > 0)
                {
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMP_ALREADY_EXISTS_MSG,
                                    ErrorFlowConstants.EMP_ALREADY_EXISTS_CODE_EM006,
                                    ErrorFlowConstants.EMP_ALREADY_EXISTS_DESC, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                    return messageROList;
                }

                EmployeeBO postEmployeeBO = handler.addEmployee(employeeRO);
                if (postEmployeeBO != null)
                {
                    addResponseData(HttpServletResponse.SC_CREATED, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMP_REG_SUCCESS_MSG,
                                    ErrorFlowConstants.EMP_REG_SUCCESS_CODE_EM004,
                                    ErrorFlowConstants.EMP_REG_SUCCESS_DESC, RestConstant.STATUS_SUCESS));
                    messageROList.setResult(list);
                }
                else
                {
                    addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMP_REG_ERROR_MSG,
                                    ErrorFlowConstants.EMP_REG_ERROR_CODE_EM005, ErrorFlowConstants.EMP_REG_ERROR_DESC,
                                    RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }

        }
        catch(Exception e)
        {
            addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "getEmployee", getName());
            list.add(new MessageRO(ErrorFlowConstants.EMP_REG_ERROR_MSG, ErrorFlowConstants.EMP_REG_ERROR_CODE_EM005,
                            ErrorFlowConstants.EMP_REG_ERROR_DESC, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource addEmployee()" + ErrorFlowConstants.EMP_REG_ERROR_MSG, e);
        }
        return messageROList;
    }

    @ApiOperation("Delete Employee")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Delete Employee",
                    response = EmployeeRO.class) })
    @DELETE
    @Path("{" + EMPLOYEE_CODE + ":[^/]*}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    public AbstractResourceObject deleteItem(@PathParam(EMPLOYEE_CODE) Integer empCode)
    {
        Logger.debug(this, " EmployeeResource deleteItem");
        List<MessageRO> list = new ArrayList<>();
        MessageROList messageROList = new MessageROList();
        setCacheExpires(0L);
        try
        {
            if (isCurrentUserLoggedIn())
            {
                EmployeeBO employeeBO = handler.getEmployee(empCode);
                if (employeeBO == null || employeeBO.getID() == null || employeeBO.getEmpCode() <= 0)
                {
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                    return messageROList;
                }

                if (handler.deleteEmployee(empCode))
                {
                    addResponseData(HttpServletResponse.SC_OK, "Employee", "..");
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_DELETED, ErrorFlowConstants.ERROR_CODE_EM003,
                                    ErrorFlowConstants.EMPLOYEE_DELETED_SUCCESSFULY, RestConstant.STATUS_SUCESS));
                    messageROList.setResult(list);
                }
                else
                {
                    addResponseData(HttpServletResponse.SC_NOT_FOUND, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Employee", getName());
            list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.EMPLOYEE_NOT_FOUND,
                            ErrorFlowConstants.ERROR_CODE_EM002, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource deleteItem()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND, e);
        }
        return messageROList;
    }

    @ApiOperation("Update Employee")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Update Employee",
                    response = EmployeeRO.class) })
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public AbstractResourceObject updateEmployee(EmployeeRO employeeRO)
    {
        Logger.debug(this, "EmployeeResource updateEmployee");
        List<MessageRO> list = new ArrayList<>();
        MessageROList messageROList = new MessageROList();
        EmployeeBO updatedEmployeeBO = null;
        try
        {
            if (isCurrentUserLoggedIn())
            {
                EmployeeBO employeeBO = handler.getEmployee(employeeRO.getEmpCode());
                if (employeeBO == null || employeeBO.getID() == null || employeeBO.getEmpCode() <= 0)
                {
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                    return messageROList;
                }

                updatedEmployeeBO = handler.updateEmployee(employeeRO);
                if (updatedEmployeeBO == null)
                {
                    addResponseData(HttpServletResponse.SC_OK, "updateEmployee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMP_UPDATE_ERROR_MSG,
                                    ErrorFlowConstants.EMP_UPDATE_ERROR_CODE_EM007,
                                    ErrorFlowConstants.EMP_UPDATE_ERROR_DESC, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                    return messageROList;
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "updateEmployee", getName());
            list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.EMPLOYEE_NOT_FOUND,
                            ErrorFlowConstants.ERROR_CODE_EM002, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource updateEmployee()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND, e);
            return messageROList;
        }
        addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
        return employeeROCollectionMapper.resolve(updatedEmployeeBO);
    }

    @SuppressWarnings("unused")
    @ApiOperation("Get Employee")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Get Employee",
                    response = EmployeeRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Employee could not be get.", response = RestException.class) })
    @GET
    @Path("code/{" + EMPLOYEE_CODE + ":[^/]*}")
    @Produces({ VERSION2 + JSON_SUFFIX })
    public AbstractResourceObject getEmployeeV2(@PathParam(EMPLOYEE_CODE) Integer empCode)
    {
        List<MessageRO> list = new ArrayList<>();
        MessageROList messageROList = new MessageROList();

        setCacheExpires(0);
        Logger.debug(this, " EmployeeResource getEmployee");

        try
        {
            if (isCurrentUserLoggedIn())
            {
                EmployeeBO employeeBO = handler.getEmployee(empCode);
                if (employeeBO != null)
                {
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                    EmployeeRO employeeRO = employeeROCollectionMapper.resolve(employeeBO);
                    String addHelo = "Hello " + employeeRO.getEmpName();
                    employeeRO.setEmpName(addHelo);
                    return employeeRO;
                }
                else
                {
                    addResponseData(HttpServletResponse.SC_OK, "getEmployee", getName());
                    list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                                    ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
                    messageROList.setResult(list);
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            addResponseData(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "getEmployee", getName());
            list.add(new MessageRO(ErrorFlowConstants.EMPLOYEE_NOT_FOUND, ErrorFlowConstants.ERROR_CODE_EM002,
                            ErrorFlowConstants.EMPLOYEE_NOT_FOUND, RestConstant.STATUS_ERROR));
            messageROList.setResult(list);
            Logger.error(this, "EmployeeResource getEmployeeV2()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND, e);
        }
        return messageROList;
    }

    @SuppressWarnings("unchecked")
    @GET
    @Produces({ VER2 + JSON_SUFFIX })
    public AbstractResourceObject getEmployeeList(@QueryParam(RestConstant.PAGEABLE_ID) String pageableID,
                    @QueryParam(RestConstant.PAGEABLE_OFFSET) Integer offset,
                    @QueryParam(RestConstant.PAGEABLE_AMOUNT) Integer amount)
    {

        Iterator<EmployeePO> employees = null;

        if (amount == null)
        {
            amount = RestConstant.PAGESIZE_DEFAULT;
        }

        if (offset == null)
        {
            offset = 0;
        }
        if (pageableID != null)
        {
            PagingMgr pagingMgr = (PagingMgr)NamingMgr.getInstance()
                            .lookupManager(PagingMgr.REGISTRY_NAME);
            employees = pagingMgr.lookupPageable(pageableID);
        }
        if (employees == null)
        {
            Integer pageSize = null;
            Integer pageNumber = null;

            pageSize = RESTUtil.calcPageSize(offset, amount);
            pageNumber = RESTUtil.calcPageNumber(offset, amount);

            employees = handler.getEmployeeListBySimpleSearch(pageSize, pageNumber);
        }
        setCacheExpires(0);
        return createOrderCollection(employees, pageableID, offset, amount);
    }

    private AbstractResourceObject createOrderCollection(Iterator<EmployeePO> employees, String pageableID,
                    Integer offset, Integer amount)
    {
        ResourceCollectionRO<EmployeeRO> employeeList = new ResourceCollectionRO<>(EMPLOYEES);
        employeeList.setPageable(pageableID);
        return null;
    }

    @GET
    @Path("salary")
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeResourceCollectionRO getEmployeeBySalaryRange(
                    @QueryParam(RestConstant.PAGEABLE_MIN_SAL) Integer minSal,
                    @QueryParam(RestConstant.PAGEABLE_MAX_SAL) Integer maxSal)
    {
        EmployeeResourceCollectionRO collectionRO = new EmployeeResourceCollectionRO();
        Logger.debug(this, " EmployeeResource getEmployeeBySalaryRange()");
        List<MessageRO> list = new ArrayList<>();
        ResourceCollectionMessageROList messageROList = new ResourceCollectionMessageROList();

        setCacheExpires(0);

        if (minSal == null)
        {
            minSal = 0;
        }

        if (maxSal == null)
        {
            maxSal = 0;
        }
        try
        {
            if (isCurrentUserLoggedIn())
            {
                List<EmployeeBO> employeeBOList = handler.getEmployeeBySalaryRange(minSal, maxSal);
                if (employeeBOList != null)
                {
                    employeeBOList.stream()
                                    .forEach(e -> {
                                        collectionRO.addElement(employeeROCollectionMapper.resolve(e));

                                    });
                    int total = (int)employeeBOList.stream()
                                    .count();
                    collectionRO.setTotal(total);
                    collectionRO.setInfo("" + total + " list items");
                    addResponseData(HttpServletResponse.SC_OK, "Employee", getName());
                }
            }
            else
            {
                addResponseData(HttpServletResponse.SC_UNAUTHORIZED, "Employee", getName());
                list.add(new MessageRO("Unauthorized Error", "401", "Access to this resource is denied",
                                RestConstant.STATUS_ERROR));
                messageROList.setResult(list);
                return messageROList;
            }
        }
        catch(Exception e)
        {
            Logger.error(this, "EmployeeResource getEmployeeBySalaryRange()" + ErrorFlowConstants.EMPLOYEE_NOT_FOUND,
                            e);

        }
        return collectionRO;
    }

    private Boolean isCurrentUserLoggedIn() throws Exception
    {
        Session session = Request.getCurrent()
                        .getSession();
        return this.getCurrentUser() != null && session.isLoggedIn();
    }

    private UserBO getCurrentUser() throws Exception
    {
        Session session = Request.getCurrent()
                        .getSession();
        User user = session.getUser();
        return user == null ? null
                        : this.getUserBORepository()
                                        .getUserBOByID(user.getID());
    }

    private UserBORepository getUserBORepository()
    {
        return (UserBORepository)this.applicationBOProvider.get()
                        .getRepository("UserBORepository");
    }

}
