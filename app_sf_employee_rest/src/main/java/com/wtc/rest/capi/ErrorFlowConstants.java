package com.wtc.rest.capi;

public final class ErrorFlowConstants
{

    public static final String ERROR_CODE_EM001 = "EM001";

    public static final String ERROR_CODE_EM002 = "EM002";

    public static final String ERROR_CODE_EM003 = "EM003";

    public static final String EMP_REG_SUCCESS_CODE_EM004 = "EM004";

    public static final String EMP_REG_ERROR_CODE_EM005 = "EM005";

    public static final String EMP_ALREADY_EXISTS_CODE_EM006 = "EM006";

    public static final String EMP_UPDATE_ERROR_CODE_EM007 = "EM007";

    public static final String LOGIN_ERROR_CODE_EM008 = "403";

    public static final String EMPLOYEE_CODE_MISSING = "Employee Code Missing";

    public static final String EMPLOYEE_NOT_FOUND = "Employee Not Found";

    public static final String EMPLOYEE_NOT_GET = "The Employee could not be get.";

    public static final String EMPLOYEE_DELETED_SUCCESSFULY = "Employee Delete successfully";

    public static final String EMPLOYEE_DELETED = "Employee Deleted";

    public static final String EMP_REG_SUCCESS_MSG = "Employee Registred";

    public static final String EMP_REG_SUCCESS_DESC = "Employee Registred successfully";

    public static final String EMP_REG_ERROR_MSG = "Employee Not Registred";

    public static final String EMP_REG_ERROR_DESC = "Employee Failed To Register";

    public static final String EMP_ALREADY_EXISTS_MSG = "Employee Already Exists";

    public static final String EMP_ALREADY_EXISTS_DESC = "Employee Already Exists";

    public static final String EMP_UPDATE_ERROR_MSG = "Employee Not updated";

    public static final String EMP_UPDATE_ERROR_DESC = "Employee could not be updated";

    public static final String LOGIN_ERROR_CODE_MSG = "Token required client error";

    public static final String LOGIN_ERROR_CODE_DESC = "Authentication credentials not provided in the request";

}
