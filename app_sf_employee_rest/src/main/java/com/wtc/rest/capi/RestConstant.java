package com.wtc.rest.capi;

public interface RestConstant
{

    public static final String PAGEABLE_ID = "pageable";

    public static final String PAGEABLE_AMOUNT = "amount";

    public static final String PAGEABLE_OFFSET = "offset";

    public static final Integer PAGESIZE_DEFAULT = 5;

    public static final String STATUS_SUCESS = "Success";

    public static final String STATUS_ERROR = "Error";

    public static final String PAGEABLE_MIN_SAL = "minSal";

    public static final String PAGEABLE_MAX_SAL = "maxSal";
}
