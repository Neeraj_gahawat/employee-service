package com.wtc.rest.capi.resourceobject;

import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;

public class ErrorMessageRO extends AbstractResourceObject
{
    private String error;
    private String message;
    private String errorCode;
    
    public ErrorMessageRO(){
        // default 
    }
    public ErrorMessageRO(String error, String message)
    {
        super();
        setName("Error");
        setType("ErrorMessageRO");
        this.error = error;
        this.message = message;
    }
    
    public ErrorMessageRO(String error, String message, String errorCode)
    {
        super();
        setName("Error");
        setType("ErrorMessageRO");
        this.error = error;
        this.message = message;
        this.errorCode = errorCode;
    }
    
    public String getError()
    {
        return error;
    }
    public void setError(String error)
    {
        this.error = error;
    }
    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }
    public String getErrorCode()
    {
        return errorCode;
    }
    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

}
