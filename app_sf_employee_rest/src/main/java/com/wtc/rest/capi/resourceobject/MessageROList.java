package com.wtc.rest.capi.resourceobject;

import java.util.List;

import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;

public class MessageROList extends AbstractResourceObject
{

    private List<MessageRO> result;

    public List<MessageRO> getResult()
    {
        return result;
    }

    public void setResult(List<MessageRO> result)
    {
        this.result = result;
    }
}
