package com.wtc.rest.internal;

import java.util.Objects;

import javax.inject.Inject;

import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.rest.capi.resourceobject.EmployeeRO;

public class EmployeeBOToEmployeeROMapperImpl implements ObjectMapper<EmployeeBO, EmployeeRO>
{

    @Inject
    BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;

    @Override
    public EmployeeRO resolve(EmployeeBO employeeBO)
    {

        Objects.requireNonNull(employeeBO);

        return new EmployeeRO(employeeBO.getEmpCode(), employeeBO.getEmpName(), employeeBO.getSalary(),
                        employeeBO.getDesignation());
    }
}
