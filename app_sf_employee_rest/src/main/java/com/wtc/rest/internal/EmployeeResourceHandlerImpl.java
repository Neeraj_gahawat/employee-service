package com.wtc.rest.internal;

import java.util.Iterator;
import java.util.List;

import com.intershop.beehive.core.capi.app.AppContextUtil;
import com.intershop.beehive.core.capi.paging.PageableIterator;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.foundation.quantity.Money;
import com.intershop.component.application.capi.ApplicationBO;
import com.intershop.component.rest.capi.pipeline.PipelineCall;
import com.intershop.component.rest.capi.pipeline.PipelineCallFactory;
import com.wtc.employee.capi.bo.EmployeeBO;
import com.wtc.employee.capi.repo.EmployeeBORepository;
import com.wtc.employee.internal.EmployeePO;
import com.wtc.rest.capi.EmployeeResourceHandler;
import com.wtc.rest.capi.resourceobject.EmployeeRO;

public class EmployeeResourceHandlerImpl implements EmployeeResourceHandler
{

    private EmployeeBORepository getRepositoryBO()
    {

        ApplicationBO currentApplicationBO = AppContextUtil.getCurrentAppContext()
                        .getVariable(ApplicationBO.CURRENT);
        return currentApplicationBO.getRepository(EmployeeBORepository.EXTENSION_ID);

    }

    @Override
    public List<EmployeeBO> getAll()
    {
        return getRepositoryBO().getAllEmployee();
    }

    @Override
    public EmployeeBO addEmployee(EmployeeRO employeeRO)
    {

        return getRepositoryBO().registerEmployee(employeeRO.getEmpCode(), employeeRO.getEmpName(),
                        new Money(Request.getCurrent()
                                        .getCurrencyCode(),
                                        employeeRO.getSalary()
                                                        .getValue()),
                        employeeRO.getDesigantion());

    }

    @Override
    public boolean deleteEmployee(Integer empCode)
    {
        return getRepositoryBO().removeEmployeeByCode(empCode);
    }

    @Override
    public EmployeeBO updateEmployee(EmployeeRO employeeRO)
    {
        EmployeeBORepository employeeBORepository = getRepositoryBO();
        EmployeeBO employeeBO = employeeBORepository.getEmployeeByEmpCode(employeeRO.getEmpCode());

        employeeBO.setEmpName(employeeRO.getEmpName());
        employeeBO.setDesignation(employeeRO.getDesigantion());
        employeeBO.setSalary(employeeRO.getSalary());

        return employeeBORepository.updateEmployee(employeeBO.getID(), employeeBO);

    }

    @Override
    public EmployeeBO getEmployee(Integer empCode)
    {
        return getRepositoryBO().getEmployeeByEmpCode(empCode);
    }

    @Override
    public Iterator<EmployeePO> getEmployeeListBySimpleSearch(Integer pageSize, Integer pageNumber)
    {
        PipelineCall pipelineCall = PipelineCallFactory.createPipelineCall("ProcessTestList", "GetTest",
                        PageableIterator.class, PipelineCall.RESULT_PIPELINE_DICT);

        PipelineDictionary dict = (PipelineDictionary)pipelineCall.invoke();
        PageableIterator<EmployeePO> sortedEmployeePOs = dict.get("SortedOrders");

        return sortedEmployeePOs;

    }

    @Override
    public List<EmployeeBO> getEmployeeBySalaryRange(Integer minSal, Integer maxSal)
    {
        return getRepositoryBO().getEmployeeBySalaryRange(minSal, maxSal);
        
    }
}
