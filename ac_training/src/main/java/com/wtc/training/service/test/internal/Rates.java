package com.wtc.training.service.test.internal;

public class Rates
{
    private Float INR;
    private Float USD;

    // Getter Methods

    public Float getINR()
    {
        return INR;
    }

    // Setter Methods

    public void setINR(Float INR)
    {
        this.INR = INR;
    }

    public Float getUSD()
    {
        return USD;
    }

    // Setter Methods

    public void setUSD(Float USD)
    {
        this.USD = USD;
    }
}