package com.wtc.training.service.test.internal;

public class TrainingRequest
{
    /**
     * @return the convertTo
     */
    public String getConvertTo()
    {
        return convertTo;
    }

    /**
     * @param convertTo the convertTo to set
     */
    public void setConvertTo(String convertTo)
    {
        this.convertTo = convertTo;
    }

    private String convertTo;
}
