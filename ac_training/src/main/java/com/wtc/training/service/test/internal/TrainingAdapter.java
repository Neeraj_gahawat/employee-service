package com.wtc.training.service.test.internal;

import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.component.service.capi.ServiceException;
import com.intershop.component.service.capi.chain.adapter.MultiOperationAdapter;
import com.intershop.component.service.capi.chain.envelope.Envelope;
import com.intershop.component.service.capi.config.ConfigurationException;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;
import com.wtc.training.service.test.capi.TrainingService;

public class TrainingAdapter extends MultiOperationAdapter implements TrainingService
{
    private static final String OPERATION_TEST = "RestTest";

    public TrainingAdapter(ServiceConfigurationBO serviceConfiguration, Class<?> serviceInterface)
    {
        super(serviceConfiguration, serviceInterface);
        
        if (!TrainingService.class.isAssignableFrom(serviceInterface))
        {
            throw new IllegalArgumentException(TrainingService.class.getName()
                            .concat(" does not support ")
                            .concat(serviceInterface.getName()).concat("."));
        }
        
        try
        {
            setExecutor(OPERATION_TEST, new TrainingServiceExecuter(getConfiguration()));
        }
        catch(ConfigurationException e)
        {
            Logger.error(this, "LoyaltyProServiceAdapter Configuration exception", e);
        }
    }

    @Override
    public TrainingResponse restTest(TrainingRequest request) throws ServiceException
    {
        Envelope<TrainingRequest, TrainingResponse> envelope = super.invoke(OPERATION_TEST, request);
        
        if (envelope.getServiceException() != null){
            throw envelope.getServiceException();
        }
        
        TrainingResponse response = envelope.getAnswer();
        
        return response;
    }
}
