package com.wtc.training.service.test.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.intershop.component.service.capi.assignment.ServiceProvider;
import com.intershop.component.service.capi.service.AbstractServiceDefinition;
import com.wtc.training.service.test.capi.TrainingService;

public class TrainingServiceDefinition extends AbstractServiceDefinition
{
    private final Collection<Class<?>> serviceInterfaces = new ArrayList<>(1);
    
    public TrainingServiceDefinition()
    {
        super(TrainingAdapter.class);
        serviceInterfaces.add(TrainingService.class);
    }
      

    @Override
    public Collection<Class<?>> getServiceInterfaces()
    {
        return serviceInterfaces;
    }
    
    protected class TrainingServiceProvider implements ServiceProvider
    {
        private final TrainingService trainingService;

        public TrainingServiceProvider(TrainingService trainingService)
        {
            this.trainingService = trainingService;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> T getServiceAdapter(Class<T> serviceInterface)
        {
            if (!serviceInterface.isAssignableFrom(TrainingService.class))
            {
                throw new IllegalArgumentException("Can't provide an implementation for requested interface: "
                                + serviceInterface.getName());
            }
            return (T)trainingService;
        }
    }
}
