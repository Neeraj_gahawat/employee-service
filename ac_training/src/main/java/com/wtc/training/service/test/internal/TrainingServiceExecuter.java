package com.wtc.training.service.test.internal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.intershop.beehive.configuration.capi.common.Configuration;
import com.intershop.component.service.capi.chain.envelope.Envelope;
import com.intershop.component.service.capi.service.ExecutorService;

public class TrainingServiceExecuter implements ExecutorService<TrainingRequest, TrainingResponse>
{
    Configuration serviceConfiguration;
    private final String URL = "TrainingService.url";

    public TrainingServiceExecuter(Configuration conf)
    {
        this.serviceConfiguration = conf;
    }

    @Override
    public void invoke(Envelope<TrainingRequest, TrainingResponse> envelope)
    {
        TrainingRequest request = envelope.getQuestion();

        // get URL and other parameters...
        String endPoint = serviceConfiguration.getString(URL);
        StringBuffer sb = new StringBuffer(endPoint);
        sb.append(request.getConvertTo());

        // create rest client and call service...
        Client client = ClientBuilder.newClient();

        // String convertTo = "INR";

        String res = client.target(sb.toString())
                           .request(MediaType.APPLICATION_JSON)
                           .get(String.class);

        Gson g = new Gson();
        envelope.receive(g.fromJson(res, TrainingResponse.class));
    }

}
