package com.wtc.training.service.test.internal;

public class TrainingResponse
{
    private Rates rates;

    /**
     * @return the rates
     */
    public Rates getRates()
    {
        return rates;
    }

    /**
     * @param rates
     *                  the rates to set
     */
    public void setRates(Rates rates)
    {
        this.rates = rates;
    }

    private String base;
    private String date;

    // Getter Methods

    public String getBase()
    {
        return base;
    }

    public String getDate()
    {
        return date;
    }

    // Setter Methods

    public void setBase(String base)
    {
        this.base = base;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
}
