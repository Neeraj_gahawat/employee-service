package com.wtc.training.service.test.internal;

import com.intershop.component.service.capi.assignment.ServiceProvider;
import com.wtc.training.service.test.capi.TrainingService;

public class TrainingServiceProvider implements ServiceProvider
{
    private final TrainingService trainingService;

    public TrainingServiceProvider(TrainingService trainingService)
    {
        this.trainingService = trainingService;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getServiceAdapter(Class<T> serviceInterface)
    {
        if (!serviceInterface.isAssignableFrom(TrainingService.class))
        {
            throw new IllegalArgumentException("Can't provide an implementation for requested interface: "
                            + serviceInterface.getName());
        }
        return (T)trainingService;
    }
}

