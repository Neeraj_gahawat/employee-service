/**
 * 
 */
package com.wtc.training.service.test.pipelet;

import java.util.Collection;

import com.intershop.beehive.core.capi.app.AppContextUtil;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.core.capi.request.Session;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContext;
import com.intershop.component.service.capi.assignment.ServiceExecutable;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;
import com.intershop.component.service.capi.service.ServiceConfigurationBORepository;
import com.wtc.training.service.test.capi.TrainingService;
import com.wtc.training.service.test.internal.TrainingAdapter;
import com.wtc.training.service.test.internal.TrainingRequest;
import com.wtc.training.service.test.internal.TrainingResponse;

/**
 * @author pgoel
 *
 */
public class GetMyExchangeRate extends Pipelet
{
    private String currencyCode = "CODE";

    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        BusinessObjectRepositoryContext repositoryContext = AppContextUtil.getCurrentAppContext()
                        .getVariable(BusinessObjectRepositoryContext.CURRENT);
        TrainingRequest request = new TrainingRequest();
        Session session = Request.getCurrent().getSession();
        
        request.setConvertTo(session.getCurrencyCode());
        TrainingResponse resp = null;
        ServiceExecutable executable = repositoryContext.getRepository(ServiceConfigurationBORepository.EXTENSION_ID);
        Collection<ServiceConfigurationBO> serviceConf = executable.getRunnableServiceConfigurationBOs(TrainingService.class);
        TrainingAdapter trainingAdapter = serviceConf.iterator().next().getServiceAdapter(TrainingAdapter.class);
        try
        {
            resp = trainingAdapter.restTest(request);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
      
        if(resp != null){
            aPipelineDictionary.put("conversionRate",resp.getRates().getUSD());
        }else{
            return PIPELET_ERROR;
        }
       
        return PIPELET_NEXT;
    }

}
