package com.wtc.training.service.test.internal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;



public class ManageTest
{

    public static void main(String[] args)
    {
       // String endPoint  = serviceConfiguration.getString(URL);
        // create rest client and call service...
        Client client = ClientBuilder.newClient();
       
       // String convertTo = "INR";
        
        String res = client
        .target("https://api.exchangeratesapi.io/latest?symbols=INR")
        //.path(convertTo)
        .request(MediaType.APPLICATION_JSON)
        .get(String.class);
        //System.out.println(res);
        Gson g = new Gson();
        TrainingResponse response= g.fromJson(res, TrainingResponse.class);
        
        System.out.println(response.getBase()); 
        System.out.println(response.getRates().getINR()); 
        System.out.println(response.getDate()); 
    }

}
