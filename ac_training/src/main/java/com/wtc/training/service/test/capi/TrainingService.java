package com.wtc.training.service.test.capi;

import com.intershop.component.service.capi.ServiceException;

import com.wtc.training.service.test.internal.TrainingRequest;
import com.wtc.training.service.test.internal.TrainingResponse;

public interface TrainingService
{
    public TrainingResponse restTest(TrainingRequest request) throws ServiceException;
}
