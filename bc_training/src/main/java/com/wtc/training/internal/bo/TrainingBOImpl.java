package com.wtc.training.internal.bo;

import java.util.Date;

import com.intershop.beehive.businessobject.capi.BusinessObjectContext;
import com.intershop.beehive.core.capi.domain.AbstractPersistentObjectBO;
import com.intershop.beehive.foundation.quantity.Money;
import com.wtc.training.capi.bo.TrainingBO;
import com.wtc.training.internal.TrainingPO;

public class TrainingBOImpl extends AbstractPersistentObjectBO<TrainingPO>
implements TrainingBO{

    public TrainingBOImpl(TrainingPO trainingPO, BusinessObjectContext context)
    {
        super(trainingPO, context);
    }

    @Override
    public String getName()
    {
       return this.getPersistentObject().getName();
    }

    @Override
    public Money getFees()
    {
        // TODO Auto-generated method stub
        return this.getPersistentObject().getFees();
    }

    @Override
    public Date getStartDate()
    {
        // TODO Auto-generated method stub
        return this.getPersistentObject().getStartDate();
    }

    @Override
    public Date getEndDate()
    {
        // TODO Auto-generated method stub
        return this.getPersistentObject().getEndDate();
    }

    @Override
    public void setName(String name)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setFees(Money fees)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setStartDate(Date d)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setEndDate(Date d)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Long getTrainingId()
    {
        // TODO Auto-generated method stub
        return this.getPersistentObject().getId();
    }

    @Override
    public void setTrainingId(Long id)
    {
        // TODO Auto-generated method stub
        
    }
    
    

}
