/**
 * 
 */
package com.wtc.training.internal.bo;

import java.util.Objects;

import com.google.inject.Inject;
import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContext;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.wtc.training.capi.bo.TrainingBO;
import com.wtc.training.capi.bo.TrainingBORepository;
import com.wtc.training.internal.TrainingPO;
/**
 * @author pgoel
 *
 */
public class TrainingPOToTrainingBOMapper implements ObjectMapper<TrainingPO, TrainingBO>
{
        @Inject
        BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;
        
        @Override
        public TrainingBO resolve(TrainingPO trainingPO){
            Objects.requireNonNull(trainingPO);
            
            BusinessObjectRepositoryContext businessObjectRepositoryContext = businessObjectRepositoryContextProvider.getBusinessObjectRepositoryContext();
            TrainingBORepository trainingBORepository = businessObjectRepositoryContext.getRepository(TrainingBORepository.EXTENSION_ID);
            
            return trainingBORepository.getTraining(trainingPO.getUUID());
            
        }
    

}
