// =============================================================================
// File: TrainingPOAlternateKey.java
// Generated by JGen Code Generator from INTERSHOP Communications AG.
// Generator template: ORMAlternateKey.xpt(checksum: 293f94602d5b490cb50422801e7a60a5)
// =============================================================================
// The JGen Code Generator software is the property of INTERSHOP Communications AG. 
// Any rights to use are granted under the license agreement. 
// =============================================================================
package com.wtc.training.internal;

import com.intershop.beehive.orm.capi.common.ORMObjectAlternateKey;

/**
 * This class represents the alternate key for objects of type TrainingPO. The key objects can be used for lookup
 * operations in the database.
 * 
 * @author pgoel
 *
 * @generated
 */
public class TrainingPOAlternateKey extends ORMObjectAlternateKey
{
    /**
     * Serialization version to make compiler happy.
     *
     * @generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * An alternate key attribute.
     *
     * @generated
     */
    private Long id;

    /**
     * Creates an empty alternate key. After creation of a new key object you must call the corresponding
     * set<i>Attribute</i> method(s) to set the value(s) of the alternate key.
     *
     * @generated
     */
    public TrainingPOAlternateKey()
    {
    }

    /**
     * Creates an alternate key with the specified value(s).
     *
     * @generated
     */
    public TrainingPOAlternateKey(Long id)
    {
        this.id = id;
    }

    /**
     * Returns the alternate key attribute.
     *
     * @return the value of the alternate key attribute
     * @generated
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets the alternate key attribute.
     *
     * @param value
     *                  the attribute value
     * @generated
     */
    public void setId(Long value)
    {
        this.id = value;
    }

    /**
     * Checks if the key is a null key, e.g. all alternate key attributes are still set to null. Such keys are ambiguous
     * and can therefore not be used for lookup operations.
     *
     * @generated
     */
    @Override
    public boolean isNullKey()
    {
        return (id == null);
    }

    /**
     * Returns the hashcode of the alternate key object.
     *
     * @return the hashcode
     * @generated
     */
    public int hashCode()
    {
        int hash = 0;

        if (id != null)
        {
            hash ^= id.hashCode();
        }

        return hash;
    }

    /**
     * Compares an object with this alternate key object.
     *
     * @return true, if the other object is an alternate key object of the same type and all key attributes are equal
     * @generated
     */
    public boolean equals(Object anObject)
    {
        if (this == anObject)
        {
            return true;
        }

        if (isNullKey())
        {
            return false;
        }

        if (anObject != null)
        {
            if (anObject instanceof TrainingPOAlternateKey)
            {
                TrainingPOAlternateKey that = (TrainingPOAlternateKey)anObject;

                if ((id != null) && !id.equals(that.id))
                {
                    return false;
                }
                if ((id == null) && (that.id != null))
                {
                    return false;
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Returns a string representation of this key object.
     *
     * @return a string containing the key attributes
     * @generated
     */
    public String toString()
    {
        StringBuilder buf = new StringBuilder("com.wtc.training.internal.TrainingPOAlternateKey[");

        buf.append(id);
        buf.append("]");

        return buf.toString();
    }
}
