package com.wtc.training.internal.bo;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.google.inject.Inject;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.series.BasicSeriesGenerator;
import com.intershop.beehive.core.capi.series.BasicSeriesGeneratorFactory;
import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.beehive.foundation.quantity.Money;
import com.intershop.beehive.orm.capi.common.ORMObjectCollection;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtension;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContext;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.training.capi.bo.TrainingBO;
import com.wtc.training.capi.bo.TrainingBORepository;
import com.wtc.training.internal.TrainingPO;
import com.wtc.training.internal.TrainingPOAlternateKey;
import com.wtc.training.internal.TrainingPOFactory;
import com.wtc.training.internal.TrainingPOKey;

public class TrainingBORepositoryImpl extends AbstractDomainRepositoryBOExtension
implements TrainingBORepository,ObjectMapper<TrainingPO, TrainingBO>
{

    @Inject
    BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;
    
    @Inject
    private BasicSeriesGeneratorFactory generatorFactory;
    
    public TrainingBORepositoryImpl(String extensionID, RepositoryBO extendedObject)
    {
        super(extensionID, extendedObject);
        NamingMgr.injectMembers(this);
    }

    @Inject
    private TrainingPOFactory trainingFactory;
    
    @Override
    public TrainingBO createTraining(String name,Money fee,Date startDate, Date endDate) {

        BasicSeriesGenerator series = null;
        Long id = null;
        if(!obtainSeriesGenerator("TrainingSequence").sequenceExists()) {
          boolean created = obtainSeriesGenerator("TrainingSequence")
                     .createDatabaseSequence(1L, 1L, 1L, 99999L, 10, false, true);
          if(created) {
              series = obtainSeriesGenerator("TrainingSequence");  
          }
          
        }
        id = series.getNextSeriesNumber();
        TrainingPO po = trainingFactory.create(getDomain(),id,name);
        //po.setName(name);
        po.setStartDate(startDate);
        po.setEndDate(endDate);
        po.setFees(fee);
        
        return new TrainingBOImpl(po, getContext());
    }
    
    @Override
    public TrainingBO updateTraining(String UUID,String name,Money fee,
                    Date startDate, Date endDate) {
        TrainingPO po =  trainingFactory.findByPrimaryKey(new TrainingPOKey(UUID));
        po.setName(name);
        po.setStartDate(startDate);
        po.setStartDate(endDate);
        po.setFees(fee);
        
        return new TrainingBOImpl(po, getContext());
    }
    
    @Override
    public void deleteTraining(String UUID) {
       trainingFactory.remove(new TrainingPOKey(UUID));
    }
    
        
    @Override
    @SuppressWarnings("unchecked")
    public List<TrainingBO> getAllTraining(){
        
       ORMObjectCollection<TrainingPO> trainingPOList = 
                       trainingFactory.getAllObjects();
       
       return trainingPOList.stream().map(po -> resolve(po))
                       .collect(Collectors.toList());
       
   }

    
    @Override
    @SuppressWarnings("unchecked")
    public TrainingBO getTraining(String UUID){
       TrainingPO po = trainingFactory.getObjectByPrimaryKey(new TrainingPOKey(UUID));
       
       return new TrainingBOImpl(po, getContext());
   }

    @Override
    public TrainingBO resolve(TrainingPO trainingPO){
        Objects.requireNonNull(trainingPO);

        BusinessObjectRepositoryContext businessObjectRepositoryContext = 
                        businessObjectRepositoryContextProvider
                        .getBusinessObjectRepositoryContext();
        TrainingBORepository trainingBORepository = businessObjectRepositoryContext
                        .getRepository(TrainingBORepository.EXTENSION_ID);

        return trainingBORepository.getTraining(trainingPO.getUUID());
        
    }

    @Override
    public TrainingBO getTrainingByName(String name)
    {
        trainingFactory.getObjectsBySQLWhere("name='"+name+"'");
        return null;
    }

    @Override
    public TrainingBO updateTraining(String id, TrainingBO trainingBO)
    {
        TrainingPO po =  trainingFactory.findByPrimaryKey(new TrainingPOKey(id));
        po.setName(trainingBO.getName());
        po.setStartDate(trainingBO.getStartDate());
        po.setStartDate(trainingBO.getEndDate());
        po.setFees(trainingBO.getFees());
        
        return new TrainingBOImpl(po, getContext());
    }

    @Override
    public TrainingBO getTrainingById(Long id)
    {
       
        TrainingPO po = trainingFactory.getObjectByAlternateKey(
                        new TrainingPOAlternateKey(id));
        return new TrainingBOImpl(po, getContext());
    }
    
    
    @Override
    public void removeTrainingByName(String name)
    {
        // TODO Auto-generated method stub
        
    }
    
    private BasicSeriesGenerator obtainSeriesGenerator(String identifier)
    {
          return  generatorFactory.createGenerator(identifier);
        
    }
}
