package com.wtc.training.internal.bo;

import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtensionFactory;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.training.capi.bo.TrainingBORepository;

public class TrainingRepositoryExtensionFactory extends AbstractDomainRepositoryBOExtensionFactory
{

    @Override
    public BusinessObjectExtension<RepositoryBO> createExtension(RepositoryBO repo)
    {
        // TODO Auto-generated method stub
        return new TrainingBORepositoryImpl(TrainingBORepository.EXTENSION_ID, repo);
    }

    @Override
    public String getExtensionID()
    {
        // TODO Auto-generated method stub
      return  TrainingBORepository.EXTENSION_ID;
    }

}
