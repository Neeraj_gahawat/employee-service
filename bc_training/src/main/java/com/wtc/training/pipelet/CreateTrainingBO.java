package com.wtc.training.pipelet;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.foundation.quantity.Money;
import com.wtc.training.capi.bo.TrainingBO;
import com.wtc.training.capi.bo.TrainingBORepository;

/**
 * @author pgoel
 *
 */
public class CreateTrainingBO extends Pipelet
{
   private static final String NAME = "Name";
   private static final String FEES = "Fee";
   private static final String STARTDATE = "StartDate";
   private static final String ENDDATE = "EndDate";
    
      
    @Override
    public int execute(PipelineDictionary dict) throws PipeletExecutionException
    {
        String name = dict.getRequired(NAME);
        String fees = dict.getOptional(FEES);
        BigDecimal fee = new BigDecimal(fees);

        Date startDate = dict.getRequired(STARTDATE);
        Date endDate = dict.getOptional(ENDDATE);
        
        String uuid = dict.getOptional("UUID");
        
        TrainingBORepository trainingBORepo = dict.getRequired("TrainingBORepository");
        TrainingBO trainingBO = null;
        if (uuid == null)
        {
            List<TrainingBO> boList = trainingBORepo.getAllTraining();
            Long trainingCount = boList.stream().filter(t ->  name.equalsIgnoreCase(t.getName()))
                            .count();
            if (trainingCount > 0)
            {
                dict.put("Error", "Record already exist with this name.");
                return PIPELET_ERROR;
            }
            else{
                 
                trainingBO = trainingBORepo.createTraining(name, 
                            new Money(Request.getCurrent().getCurrencyCode(), fee),
                                startDate, endDate);
                dict.put("Success", "Record Added Successfully.");
            }

        }
        else
        {
            trainingBORepo.updateTraining(uuid, name, new Money(Request.getCurrent().getCurrencyCode(), fee), 
                            startDate,endDate);
            dict.put("Success", "Record Updated Successfully.");
        }

        if (null == trainingBO)
        {
            return PIPELET_ERROR;
        }

        dict.put("TrainingBO", trainingBO);

        return PIPELET_NEXT;
    }

    
    
}
