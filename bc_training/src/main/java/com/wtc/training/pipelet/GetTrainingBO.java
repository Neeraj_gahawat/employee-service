/**
 * 
 */
package com.wtc.training.pipelet;

import java.util.ArrayList;
import java.util.List;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.training.capi.bo.TrainingBO;
import com.wtc.training.capi.bo.TrainingBORepository;

/**
 * Input:UUID
 * Pipelet to get specific Training BO.
 */

public class GetTrainingBO extends Pipelet
{

    private static final String REPO = "TrainingBORepository";
    private static final String UUID = "UUID";
    private static final String NAME = "name";

    @Override
    public int execute(PipelineDictionary dict) throws PipeletExecutionException
    {
        List<TrainingBO> boList = new ArrayList<>();

        TrainingBORepository repo = dict.getRequired(REPO);
        String uuid = dict.getOptional(UUID);
        String name = dict.getOptional(NAME);
        if(uuid == null){
            boList = repo.getAllTraining();
        }else if(name != null){
            boList.add(repo.getTrainingByName(name));
        }else{
            boList.add(repo.getTraining(uuid));
        }
        
        if (null == boList)
        {
            return PIPELET_ERROR;
        }

        dict.put("TrainingBO", repo.getTraining(uuid));
        return PIPELET_NEXT;
    }

}
