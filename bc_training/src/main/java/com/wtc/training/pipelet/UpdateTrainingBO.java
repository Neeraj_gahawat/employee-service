/**
 * 
 */
package com.wtc.training.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.training.capi.bo.TrainingBORepository;
import com.wtc.training.internal.TrainingPO;

/**
 * @author pgoel
 * Pipelet for Update training
 */
public class UpdateTrainingBO extends Pipelet
{

    @Override
    public int execute(PipelineDictionary dict) throws PipeletExecutionException
    {
        TrainingPO po = dict.getRequired("TrainingPO");
        String uuid = dict.getRequired("UUID");
        
        
        TrainingBORepository trainingBORepo = dict.getRequired("TrainingBORepository");
       
        
        
        /*TrainingBO bo = trainingBORepo.updateTraining(uuid, po);
        
        if(null == bo)
        {
            return PIPELET_ERROR;
        }*/
        
        dict.put("TrainingBO", null);
        
        return PIPELET_NEXT;
    }

}
