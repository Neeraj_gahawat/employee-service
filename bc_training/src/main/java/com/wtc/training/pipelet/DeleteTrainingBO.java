package com.wtc.training.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.foundation.util.ResettableIteratorImpl;
import com.wtc.training.capi.bo.TrainingBORepository;

public class DeleteTrainingBO extends Pipelet
{

    private static final String UUID = "UUIDs";
    
   // private static final String TRAININGBO = "TrainingBORepository";
    @Override
    public int execute(PipelineDictionary dict) throws PipeletExecutionException
    {
        TrainingBORepository repo = dict.getRequired("TrainingBORepository");
        ResettableIteratorImpl<String> uuidList = dict.getRequired(UUID);
        uuidList.forEachRemaining( e-> {
                repo.deleteTraining(e);
        });
        
        dict.put("Success", "Record Deleted Successfully.");

        return PIPELET_NEXT;
    }

}
