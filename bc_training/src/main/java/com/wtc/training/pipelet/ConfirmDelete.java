/**
 * 
 */
package com.wtc.training.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.foundation.util.ResettableIteratorImpl;

/**
 * @author pgoel
 *
 */
public class ConfirmDelete extends Pipelet
{

    @Override
    public int execute(PipelineDictionary dict) throws PipeletExecutionException
    {
       ResettableIteratorImpl<String> uuids = dict.getRequired("UUIDs");
       
       if(uuids.hasNext()){
           dict.put("ConfirmDelete", Boolean.TRUE);
       }else{
          dict.put("selectMessage", "Please select atleast one check box for delete !!"); 
       }

        return PIPELET_NEXT;
    }

}
