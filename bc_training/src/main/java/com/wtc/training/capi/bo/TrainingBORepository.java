package com.wtc.training.capi.bo;

import java.util.Date;
import java.util.List;

import com.intershop.beehive.businessobject.capi.BusinessObjectRepository;
import com.intershop.beehive.foundation.quantity.Money;

public interface TrainingBORepository extends BusinessObjectRepository
{
    public static final String EXTENSION_ID = "TrainingBORepository";

    public TrainingBO createTraining(String name,Money fee,Date startDate, Date endDate);
    
    public void deleteTraining(String UUID) ;
    
   public List<TrainingBO> getAllTraining();
   
   public TrainingBO getTraining(String id);

   public TrainingBO updateTraining(String UUID, String name, Money fee, 
                                    Date startDate, Date endDate);

   public TrainingBO getTrainingByName(String name);

   public TrainingBO updateTraining(String id, TrainingBO trainingBO);

    public void removeTrainingByName(String name);
    
    public TrainingBO getTrainingById(Long id);
}
