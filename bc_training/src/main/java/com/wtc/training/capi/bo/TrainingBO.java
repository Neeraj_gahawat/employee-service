package com.wtc.training.capi.bo;

import java.util.Date;

import com.intershop.beehive.businessobject.capi.BusinessObject;
import com.intershop.beehive.foundation.quantity.Money;

public interface TrainingBO extends BusinessObject
{
    public Long getTrainingId();
    public void setTrainingId(Long id);
    public String getName();
    
    public Money getFees();
    
    public Date getStartDate();
    
    public Date getEndDate();
    
    public void setName(String name);
    public void setFees(Money fees);
    public void setStartDate(Date d);
    public void setEndDate(Date d);
}
