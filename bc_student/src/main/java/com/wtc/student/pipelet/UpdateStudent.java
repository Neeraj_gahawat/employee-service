/**
 * 
 */
package com.wtc.student.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

/**
 * This pipelet updates the selected students data
 */

public class UpdateStudent extends Pipelet
{
    private static final String SUCCESS_MESSAGE = "Student Data Updated Successfully";
    private static final String ERROR_MESSAGE = "Student not Updated";
    @Override
    public int execute(PipelineDictionary dictionary) throws PipeletExecutionException
    {
        String uuid = dictionary.getRequired("UUID");
        String course = dictionary.getRequired("Course");
        String address = dictionary.getRequired("Address");
        String name = dictionary.getRequired("Name");
        
        StudentBORepository studentBORepository = dictionary.getRequired("StudentBORepository");
        
        StudentBO studentBO = studentBORepository.getStudentBOByUUID(uuid); 
        studentBO.setCourse(course);
        studentBO.setAddress(address);
        studentBO.setSName(name);
        
        dictionary.put("StudentBO", studentBO);
        dictionary.put("message", SUCCESS_MESSAGE);
        
        return PIPELET_NEXT;
    }

}
