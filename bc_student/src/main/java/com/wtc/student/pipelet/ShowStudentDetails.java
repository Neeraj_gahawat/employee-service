/**
 * 
 */
package com.wtc.student.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

/**
 * This pipelet retrieves full details of a student from the database using UUID and renders to the client.
 */

public class ShowStudentDetails extends Pipelet
{

    private static final String STUDENT_BO_REPOSITORY = "StudentBORepository";
    private static final String UUID2 = "UUID";
    private static final String STUDENT_BO = "StudentBO";

    @Override
    public int execute(PipelineDictionary dictionary) throws PipeletExecutionException
    {
        String uuid = dictionary.getOptional(UUID2);
        StudentBORepository studentBORepository = dictionary.getRequired(STUDENT_BO_REPOSITORY);
        
        StudentBO studentBO = studentBORepository.getStudentBOByUUID(uuid);         
        dictionary.put(STUDENT_BO, studentBO);
        
        return PIPELET_NEXT;
    }

}
