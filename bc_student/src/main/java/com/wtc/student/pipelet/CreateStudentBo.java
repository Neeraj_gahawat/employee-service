/**
 * 
 */
package com.wtc.student.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

/**
 * Creates and returns the Student BO object
 */

public class CreateStudentBo extends Pipelet
{
    private static final String STUDENT_BO_REPOSITORY = "StudentBORepository";
    private static final String STUDENT_BO = "studentBO";
    private static final String SID = "Sid";
    private static final String SNAME = "Name";
    private static final String COURSE = "Course";
    private static final String ADDRESS = "Address";
    private static final String SUCCESS_MESSAGE = "Student created successfully";
    private static final String ERROR_MESSAGE = "Student not created";
    
    @Override
    public int execute(PipelineDictionary dictionary) throws PipeletExecutionException
    {
        int sid = dictionary.getRequired(SID);
        String name = dictionary.getRequired(SNAME);
        String course = dictionary.getOptional(COURSE);
        String address = dictionary.getOptional(ADDRESS);
        
        StudentBORepository studentBORepository = dictionary.getRequired(STUDENT_BO_REPOSITORY);
        
        StudentBO studentBO = studentBORepository.createStudentBo(sid, name, course, address);
        
        if(studentBO == null) {
            dictionary.put("message", ERROR_MESSAGE);
            return PIPELET_ERROR;
        }
        
        dictionary.put(STUDENT_BO, studentBO);
        dictionary.put("UUID", studentBO.getID());
        dictionary.put("message", SUCCESS_MESSAGE);
        
        return PIPELET_NEXT;
    }

}
