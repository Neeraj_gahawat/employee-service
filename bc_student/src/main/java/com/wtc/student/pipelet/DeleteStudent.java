package com.wtc.student.pipelet;

import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.foundation.util.ResettableIterator;
import com.wtc.student.capi.StudentBORepository;

public class DeleteStudent extends Pipelet
{
    private static final String FAILURE_MESSAGE = "Student record could not be deleted";
    private static final String SUCCESS_MESSAGE = "Student record deleted successfully";
    
    @Override
    public int execute(PipelineDictionary dictionary) throws PipeletExecutionException
    {
        StudentBORepository studentBORepository = dictionary.getRequired("StudentBORepository");
        ResettableIterator uuidList = dictionary.getRequired("UUIDList");
        
        if(uuidList == null) {
            dictionary.put("message", FAILURE_MESSAGE);
        }
        while(uuidList.hasNext()) {
           String uuid = (String) uuidList.next();
           studentBORepository.deleteStudentBO(uuid);
           dictionary.put("message", SUCCESS_MESSAGE);
        } 
        
       
        return PIPELET_NEXT;
    }

}
