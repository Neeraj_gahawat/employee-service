package com.wtc.student.capi;

import com.intershop.beehive.businessobject.capi.BusinessObject;

public interface StudentBO extends BusinessObject
{
    public int getSid();
    public void setSid(int aValue);
    public String getSName();
    public void setSName(String aValue);
    public String getCourse();
    public void setCourse(String aValue);
    public String getAddress();
    public void setAddress(String aValue);
}
