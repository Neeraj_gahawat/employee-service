package com.wtc.student.capi;

import java.util.List;

import com.intershop.beehive.businessobject.capi.BusinessObjectRepository;

public interface StudentBORepository extends BusinessObjectRepository
{  
    /**
     * extension ID
     */
    public static final String EXTENSION_ID = "StudentBORepository";
    
    StudentBO createStudentBo(int sid, String name, String course, String Address);
    public void deleteStudentBO(String  uuid);
    public StudentBO getStudentBOByUUID(String uuid);
    public void updateStudent(String uuid,String name, String course,String address);
    public List<StudentBO> getAllStudents();

}
