// =============================================================================
// File: StudentPO.java
// Generated by JGen Code Generator from INTERSHOP Communications AG.
// Generator template: ORMClass.xpt(checksum: 9a846fb10e9c3b98b1a62ea7a508da27)
// =============================================================================
// The JGen Code Generator software is the property of INTERSHOP Communications AG. 
// Any rights to use are granted under the license agreement. 
// =============================================================================
package com.wtc.student;

import com.intershop.beehive.core.capi.domain.AttributeValue;
import com.intershop.beehive.core.capi.domain.ExtensibleObjectPO;
import com.intershop.beehive.orm.capi.common.ORMObjectFactory;
import com.intershop.beehive.orm.capi.description.AttributeDescription;
import com.intershop.beehive.orm.capi.description.RelationDescription;
import com.wtc.student.capi.Student;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author anyadav
 * @generated
 */
public class StudentPO extends ExtensibleObjectPO implements Student
{

    /**
     * @generated
     */
    public static AttributeDescription sidAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription nameAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription courseAttribute = null;
    /**
     * @generated
     */
    public static AttributeDescription addressAttribute = null;

    /**
     * @generated
     */
    public static RelationDescription attributeValuePOsRelation = null;

    /**
     * The constructor.
     *
     * @generated
     */
    public StudentPO(ORMObjectFactory factory, StudentPOKey key)
    {
        super(factory, key);
    }

    /**
     * Returns the name of the factory that manages this type of objects. The name can be used to lookup the factory
     * from the NamingMgr.
     *
     * @return the factory name
     * @generated
     */
    public String getFactoryName()
    {
        return getClass().getName();
    }

    /**
     * Loads the state of the object from the database.
     *
     * @deprecated use refresh now
     * @generated
     */
    public void load()
    {
        refresh();
    }

    /**
     * This hook is called whenever the bean has been modified.
     *
     * @generated modifiable
     */
    public void onChange()
    {
        // {{ bean_onchange
        // put your custom onChange code here
        // }} bean_onchange

        super.onChange();
    }

    /**
     * Returns the value of attribute 'sid'.
     * <p>
     *
     * @return the value of the attribute 'sid'
     * @generated
     */
    public int getSid()
    {
        Integer value = (Integer)getAttributeValue(sidAttribute);
        return (value != null) ? value.intValue() : (int)0;
    }

    /**
     * Sets the value of the attribute 'sid'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setSid(int aValue)
    {

        if (setAttributeValue(sidAttribute, new Integer(aValue)))
        {
            onChange();
        }
    }

    /**
     * Returns the value of attribute 'name'.
     * <p>
     *
     * @return the value of the attribute 'name'
     * @generated
     */
    public String getName()
    {
        String value = (String)getAttributeValue(nameAttribute);
        return (value != null) ? value : "";
    }

    /**
     * Sets the value of the attribute 'name'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setName(String aValue)
    {

        if (setAttributeValue(nameAttribute, aValue))
        {
            onChange();
        }
    }

    /**
     * Checks whether the value of the attribute 'name' is null.
     * <p>
     *
     * @return true if the value of attribute 'name' is null, false otherwise
     * @generated
     */
    public boolean getNameNull()
    {
        return (getAttributeValue(nameAttribute) == null);
    }

    /**
     * Sets the value of the attribute 'name' to null.
     * <p>
     *
     * @param aFlag meaningless
     * @generated
     */
    public void setNameNull(boolean aFlag)
    {

        if (setAttributeValue(nameAttribute, null))
        {

            onChange();
        }
    }

    /**
     * Returns the value of attribute 'course'.
     * <p>
     *
     * @return the value of the attribute 'course'
     * @generated
     */
    public String getCourse()
    {
        String value = (String)getAttributeValue(courseAttribute);
        return (value != null) ? value : "";
    }

    /**
     * Sets the value of the attribute 'course'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setCourse(String aValue)
    {

        if (setAttributeValue(courseAttribute, aValue))
        {
            onChange();
        }
    }

    /**
     * Checks whether the value of the attribute 'course' is null.
     * <p>
     *
     * @return true if the value of attribute 'course' is null, false otherwise
     * @generated
     */
    public boolean getCourseNull()
    {
        return (getAttributeValue(courseAttribute) == null);
    }

    /**
     * Sets the value of the attribute 'course' to null.
     * <p>
     *
     * @param aFlag meaningless
     * @generated
     */
    public void setCourseNull(boolean aFlag)
    {

        if (setAttributeValue(courseAttribute, null))
        {

            onChange();
        }
    }

    /**
     * Returns the value of attribute 'address'.
     * <p>
     *
     * @return the value of the attribute 'address'
     * @generated
     */
    public String getAddress()
    {
        String value = (String)getAttributeValue(addressAttribute);
        return (value != null) ? value : "";
    }

    /**
     * Sets the value of the attribute 'address'.
     * <p>
     *
     * @param aValue the new value of the attribute
     * @generated
     */
    public void setAddress(String aValue)
    {

        if (setAttributeValue(addressAttribute, aValue))
        {
            onChange();
        }
    }

    /**
     * Checks whether the value of the attribute 'address' is null.
     * <p>
     *
     * @return true if the value of attribute 'address' is null, false otherwise
     * @generated
     */
    public boolean getAddressNull()
    {
        return (getAttributeValue(addressAttribute) == null);
    }

    /**
     * Sets the value of the attribute 'address' to null.
     * <p>
     *
     * @param aFlag meaningless
     * @generated
     */
    public void setAddressNull(boolean aFlag)
    {

        if (setAttributeValue(addressAttribute, null))
        {

            onChange();
        }
    }

    /**
     * Returns all associated objects via the relation 'attributeValuePOs'.
     *
     * @return the enumeration of associated objects
     * @generated
     */
    @SuppressWarnings("unchecked")
    public Collection<StudentPOAttributeValuePO> getAttributeValuePOs()
    {
        return getRelatedObjects(attributeValuePOsRelation);
    }

    /**
     * Checks whether the specified element participates in the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @param anElement the element to check for participation
     * @return true, if the element is part of the relationship, false otherwise
     * @generated
     */
    public boolean isInAttributeValuePOs(StudentPOAttributeValuePO anElement)
    {
        return getAttributeValuePOs().contains(anElement);
    }

    /**
     * Determines the number of elements participating in the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @return the number of elements participating in the relation
     * @generated
     */
    public int getAttributeValuePOsCount()
    {
        return getAttributeValuePOs().size();
    }

    /**
     * Creates an iterator containing the elements of the relationship.
     * <p>
     * The attribute values of an extensible object.
     *
     * @return the iterator with the elements of the relation
     * @generated
     */
    public Iterator<StudentPOAttributeValuePO> createAttributeValuePOsIterator()
    {
        return getAttributeValuePOs().iterator();
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @param anElement the element to check for participation
     * @return true, if the element is part of the relationship, false otherwise
     * @generated
     */
    public boolean isInAttributeValues(AttributeValue anElement)
    {
        return isInAttributeValuePOs((StudentPOAttributeValuePO)anElement);
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @return the number of elements participating in the relation
     * @generated
     */
    public int getAttributeValuesCount()
    {
        return getAttributeValuePOsCount();
    }

    /**
     * Convenience wrapper that implements the public API relation 'attributeValues'. The implementation just forwards
     * the call to the internal relation implementation 'attributeValuePOs'. The attribute values of an extensible
     * object.
     *
     * @return the iterator with the elements of the relation
     * @generated
     */
    @SuppressWarnings("unchecked")
    public Iterator<AttributeValue> createAttributeValuesIterator()
    {
        return (Iterator)createAttributeValuePOsIterator();
    }

}
