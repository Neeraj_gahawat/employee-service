package com.wtc.student.bo;

import com.intershop.beehive.businessobject.capi.BusinessObjectContext;
import com.intershop.beehive.core.capi.domain.AbstractPersistentObjectBO;
import com.wtc.student.StudentPO;
import com.wtc.student.capi.StudentBO;

public class StudentBOImpl extends AbstractPersistentObjectBO<StudentPO> implements StudentBO
{

    public StudentBOImpl(StudentPO delegate, BusinessObjectContext context)
    {
        super(delegate, context);
        // TODO Auto-generated constructor stub
    }

    @Override
    public int getSid()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getSid();
    }

    @Override
    public void setSid(int aValue)
    {
        // TODO Auto-generated method stub
        getPersistentObject().setSid(aValue);
    }

    @Override
    public String getSName()
    {
        // TODO Auto-generated method stub
        return getPersistentObject().getName();
    }

    @Override
    public void setSName(String aValue)
    {
        getPersistentObject().setName(aValue);
        
    }

    @Override
    public String getCourse()
    {
        return getPersistentObject().getCourse();
    }

    @Override
    public void setCourse(String aValue)
    {
        getPersistentObject().setCourse(aValue);
        
    }

    @Override
    public String getAddress()
    {
        return getPersistentObject().getAddress();
      
    }

    @Override
    public void setAddress(String aValue)
    {
        getPersistentObject().setAddress(aValue);
        
    }

    
}
