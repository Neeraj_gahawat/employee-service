package com.wtc.student.bo;

import java.util.Objects;

import javax.inject.Inject;

import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContext;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.wtc.student.StudentPO;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

public class StudentPOToStudentBOObjectMapperImpl implements ObjectMapper<StudentPO, StudentBO>
{

    @Inject
    BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;

    @Override
    public StudentBO resolve(StudentPO studentPO)
    {
        Objects.requireNonNull(studentPO);
        
        BusinessObjectRepositoryContext businessObjectRepositoryContext = businessObjectRepositoryContextProvider.getBusinessObjectRepositoryContext();
        StudentBORepository studentBORepository = businessObjectRepositoryContext.getRepository(StudentBORepository.EXTENSION_ID);
        
        StudentBO studentBO = studentBORepository.getStudentBOByUUID(studentPO.getUUID());
        return studentBO;
    }

}
