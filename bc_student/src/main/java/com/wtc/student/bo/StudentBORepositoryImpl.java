package com.wtc.student.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.series.BasicSeriesGenerator;
import com.intershop.beehive.core.capi.series.BasicSeriesGeneratorFactory;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtension;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.student.StudentPO;
import com.wtc.student.StudentPOFactory;
import com.wtc.student.StudentPOKey;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

public class StudentBORepositoryImpl extends AbstractDomainRepositoryBOExtension implements StudentBORepository
{
    public static String TRAINING_SEQUENCE = "TrainingSequence";
    
    @Inject
    private StudentPOFactory factory;

    @Inject
    private BasicSeriesGeneratorFactory generatorFactory;

    public StudentBORepositoryImpl(String extensionID, RepositoryBO extendedObject)
    {
        super(extensionID, extendedObject);
        NamingMgr.injectMembers(this);
    }

    @Override
    public StudentBO createStudentBo(int sid, String name, String course, String address)
    {

        StudentPO studentPO = factory.create(getDomain(), getSeriesNumber().intValue());
        studentPO.setName(name);
        studentPO.setCourse(course);
        studentPO.setAddress(address);
        return new StudentBOImpl(studentPO, getContext());
    }

    @Override
    public void deleteStudentBO(String uuid)
    {
        factory.remove(new StudentPOKey(uuid));

    }

    @Override
    public StudentBO getStudentBOByUUID(String uuid)
    {
        return new StudentBOImpl(factory.getObjectByPrimaryKey(new StudentPOKey(uuid)), getContext());
    }

    @Override
    public void updateStudent(String uuid, String name, String course, String address)
    {
        StudentBO student = getStudentBOByUUID(uuid);
        student.setSName(name);
        student.setCourse(course);
        student.setAddress(address);

    }

    @Override
    public List<StudentBO> getAllStudents()
    {
        List<StudentPO> studentPOList = (List<StudentPO>)factory.getAllObjects()
                        .stream()
                        .collect(Collectors.toList());
        List<StudentBO> studentBOList = new ArrayList<>();

        for (StudentPO studentPO : studentPOList)
        {
            studentBOList.add(new StudentBOImpl(studentPO, getContext()));
        }

        return studentBOList;
    }

    private Long getSeriesNumber()
    {
        BasicSeriesGenerator series = null;
        if (!obtainSeriesGenerator(TRAINING_SEQUENCE).sequenceExists())
        {
            boolean created = obtainSeriesGenerator(TRAINING_SEQUENCE).createDatabaseSequence(1L, 1L, 1L, 99999L, 10,
                            false, true);
            if (created)
            {
                series = obtainSeriesGenerator(TRAINING_SEQUENCE);
            }
        }
        return series.getNextSeriesNumber();
    }

    private BasicSeriesGenerator obtainSeriesGenerator(String identifier)
    {
        return generatorFactory.createGenerator(identifier);
    }
}
