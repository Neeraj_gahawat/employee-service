package com.wtc.student.bo;

import com.intershop.beehive.businessobject.capi.BusinessObjectExtension;
import com.intershop.component.repository.capi.AbstractDomainRepositoryBOExtensionFactory;
import com.intershop.component.repository.capi.RepositoryBO;
import com.wtc.student.capi.StudentBORepository;

public class StudentRepositoryExtensionFactory extends AbstractDomainRepositoryBOExtensionFactory
{

    @Override
    public BusinessObjectExtension<RepositoryBO> createExtension(RepositoryBO object)
    {
        return new StudentBORepositoryImpl(StudentBORepositoryImpl.EXTENSION_ID, object);
    }

    @Override
    public String getExtensionID()
    {
        return StudentBORepository.EXTENSION_ID;
    }

}
