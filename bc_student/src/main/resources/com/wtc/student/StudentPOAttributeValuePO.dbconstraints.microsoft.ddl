/*
 =============================================================================
 File: StudentPOAttributeValuePO.dbconstraints.microsoft.ddl
 Generated by JGen Code Generator from INTERSHOP Communications AG.
 =============================================================================
 The JGen Code Generator software is the property of INTERSHOP Communications AG. 
 Any rights to use are granted under the license agreement. 
 =============================================================================
 */

PROMPT /* Class com.wtc.student.StudentPOAttributeValuePO */
EXEC staging_ddl.add_constraint('STUDENT_AV', 'STUDENT_AV_CO_002', 'FOREIGN KEY (OWNERID) REFERENCES STUDENT (UUID)');
 
 