/*
 =============================================================================
 File: StudentPO.dbindex.oracle.ddl
 Generated by JGen Code Generator from INTERSHOP Communications AG.
 =============================================================================
 The JGen Code Generator software is the property of INTERSHOP Communications AG. 
 Any rights to use are granted under the license agreement. 
 =============================================================================
 */

SET ECHO ON

SET SERVEROUTPUT ON SIZE 1000000

VARIABLE table_space      VARCHAR2(50)
VARIABLE recreate_indexes NUMBER;

EXEC :table_space := '&index_tablespace';
EXEC :recreate_indexes := '&recreate_indexes';

PROMPT /* Class com.wtc.student.StudentPO */

PROMPT -- Foreign key indices (dependencies)
EXEC staging_ddl.create_index('STUDENT_FK999', 'STUDENT', '(DOMAINID)', :table_space, 'NONUNIQUE', :recreate_indexes);


PROMPT -- Inversion Entry key indices
EXEC staging_ddl.create_index('STUDENT_IE001', 'STUDENT', '(SID)', :table_space, 'UNIQUE', :recreate_indexes);


PROMPT -- Searchable attribute indices
EXEC staging_ddl.create_index('STUDENT_IE999', 'STUDENT', '(DOMAINID)', :table_space, 'NONUNIQUE', :recreate_indexes);


EXEC staging_ddl.create_index('STUDENT_AK001', 'STUDENT', '(SID)', :table_space, 'NONUNIQUE', :recreate_indexes);

PROMPT /* Class com.wtc.student.StudentPOAttributeValue */
PROMPT -- Foreign key indices
EXEC staging_ddl.create_index('STUDENT_AV_FK002', 'STUDENT_AV', '(ownerID)', :table_space, 'NONUNIQUE', :recreate_indexes);

PROMPT -- Inversion Entry key indices
EXEC staging_ddl.create_index('STUDENT_AV_IE002', 'STUDENT_AV', '(intValue)', :table_space, 'NONUNIQUE', :recreate_indexes);
EXEC staging_ddl.create_index('STUDENT_AV_IE003', 'STUDENT_AV', '(doubleValue)', :table_space, 'NONUNIQUE', :recreate_indexes);



 