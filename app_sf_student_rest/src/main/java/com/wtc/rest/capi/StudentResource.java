package com.wtc.rest.capi;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.intershop.component.rest.capi.RestException;
import com.intershop.component.rest.capi.resource.AbstractRestResource;
import com.intershop.component.rest.capi.resourceobject.ResourceCollectionRO;
import com.intershop.component.rest.capi.response.ResponseStatusConstants;
import com.wtc.rest.capi.resourceobject.StudentRO;
import com.wtc.rest.internal.StudentBOToStudentROMapperImpl;
import com.wtc.student.capi.StudentBO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition(basePath = "Student", info = @Info(version = "1.0", title = "Student Domains and Student Jobs",
                description = "Student REST API allows to retrieve information about domains and jobs."))
@Api("Student Operation")
public class StudentResource extends AbstractRestResource
{
    public static final String STUDENT_ID = "sid";
    private StudentResourceHandler handler;

    @Inject
    private StudentBOToStudentROMapperImpl studentBOToStudentROMapperImpl;

    public StudentResource()
    {
        super();
    }

    public StudentResourceHandler getHandler()
    {
        return handler;
    }

    public void setHandler(StudentResourceHandler handler)
    {
        this.handler = handler;
    }

    @Override
    public StudentResource getRequestSpecificCopy(ResourceContext rc)
    {

        StudentResource result = (StudentResource)super.getRequestSpecificCopy(rc);
        result.setHandler(this.handler);
        return result;
    }

    @ApiOperation(value = "Get All Students", response = StudentRO.class)
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = ResponseStatusConstants.OK_MESSAGE),
                    @ApiResponse(code = ResponseStatusConstants.NOT_FOUND,
                                    message = ResponseStatusConstants.NOT_FOUND_MESSAGE) })

    @GET
    @Produces("application/json")
    public ResourceCollectionRO<StudentRO> getAllStudents()
    {
        setCacheExpires(0);
        ResourceCollectionRO<StudentRO> studentROList = new ResourceCollectionRO<>();

        List<StudentBO> studentBOs = handler.getAll();

        for (StudentBO studentBO : studentBOs)
        {

            studentROList.addElement(studentBOToStudentROMapperImpl.resolve(studentBO));
        }
        addResponseData(HttpServletResponse.SC_OK, "Student", getName());
        return studentROList;
    }

    @ApiOperation("Get Student")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Get Student",
                    response = StudentRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Student could not be get.", response = RestException.class),
                    @ApiResponse(code = ResponseStatusConstants.FORBIDDEN,
                                    message = ResponseStatusConstants.FORBIDDEN_MESSAGE,
                                    response = RestException.class) })
    @GET
    @Path("{" + STUDENT_ID + ":[^/]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public StudentRO getStudent(@PathParam(STUDENT_ID) String uuid)
    {
        setCacheExpires(0);
        StudentBO studentBO = handler.getStudent(uuid);
        addResponseData(HttpServletResponse.SC_OK, "Student", getName());
        return studentBOToStudentROMapperImpl.resolve(studentBO);
    }

    @ApiOperation("Add Student")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.CREATED, message = "Add Student",
                    response = StudentRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Student could not be add.", response = RestException.class),
                    @ApiResponse(code = ResponseStatusConstants.FORBIDDEN,
                                    message = ResponseStatusConstants.FORBIDDEN_MESSAGE,
                                    response = RestException.class) })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addStudent(StudentRO studentRO)
    {

        handler.addStudent(studentRO);
        setCacheExpires(0L);
        addResponseData(HttpServletResponse.SC_CREATED, "Student", getName());
    }

    @ApiOperation("Delete Student")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Delete Student",
                    response = StudentRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Employee could not be Deletes.", response = RestException.class),
                    @ApiResponse(code = ResponseStatusConstants.FORBIDDEN,
                                    message = ResponseStatusConstants.FORBIDDEN_MESSAGE,
                                    response = RestException.class) })
    @DELETE
    @Path("{" + STUDENT_ID + ":[^/]*}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    public void deleteItem(@PathParam(STUDENT_ID) String uuid)
    {
        handler.deleteStudent(uuid);
        setCacheExpires(0L);
        addResponseData(HttpServletResponse.SC_OK, "Student", "..");
    }

    @ApiOperation("Update Student")
    @ApiResponses({ @ApiResponse(code = ResponseStatusConstants.OK, message = "Update Student",
                    response = StudentRO.class),
                    @ApiResponse(code = ResponseStatusConstants.INTERNAL_SERVER_ERROR,
                                    message = "The Student could not be updated.", response = RestException.class),
                    @ApiResponse(code = ResponseStatusConstants.FORBIDDEN,
                                    message = ResponseStatusConstants.FORBIDDEN_MESSAGE,
                                    response = RestException.class) })
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateEmployee(StudentRO studentRO)
    {
        handler.updateStudent(studentRO);
        addResponseData(HttpServletResponse.SC_OK, "Student", getName());
        // return studentBOToStudentROMapperImpl.resolve(studentBO);
    }

}
