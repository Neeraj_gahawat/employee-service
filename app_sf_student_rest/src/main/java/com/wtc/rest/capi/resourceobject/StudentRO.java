package com.wtc.rest.capi.resourceobject;

import com.intershop.component.rest.capi.resourceobject.AbstractResourceObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Student RO")
public class StudentRO extends AbstractResourceObject
{

    private Integer sid;
    private String uuid;
    private String name;
    private String course;
    private String address;

    public StudentRO()
    {

    }

    public StudentRO(Integer sid, String name, String course, String address, String uuid)
    {
        super();
        this.sid = sid;
        this.name = name;
        this.course = course;
        this.address = address;
        this.uuid = uuid;
    }

    @ApiModelProperty(value = "uuid", example = "zyOsEYXLHIMAAAFt5.Ud1PJF")
    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @ApiModelProperty(value = "Sid", example = "101")
    public Integer getSid()
    {
        return sid;
    }

    public void setSid(Integer sid)
    {
        this.sid = sid;
    }

    @Override
    @ApiModelProperty(value = "Name", example = "Ankit")
    public String getName()
    {
        return name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @ApiModelProperty(value = "Course", example = "Java")
    public String getCourse()
    {
        return course;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    public String getAddress()
    {
        return address;
    }

    @ApiModelProperty(value = "Address", example = "India")
    public void setAddress(String address)
    {
        this.address = address;
    }

}
