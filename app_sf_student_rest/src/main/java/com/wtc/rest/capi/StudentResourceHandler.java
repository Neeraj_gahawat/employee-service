package com.wtc.rest.capi;

import java.util.List;

import com.wtc.rest.capi.resourceobject.StudentRO;
import com.wtc.student.capi.StudentBO;

public interface StudentResourceHandler
{
    public List<StudentBO> getAll();

    public StudentBO getStudent(String uuid);

    public void addStudent(StudentRO studentRO);

    public void deleteStudent(String uuid);

    public void updateStudent(StudentRO studentRO);

    public StudentBO getStudent(Integer sid);

}
