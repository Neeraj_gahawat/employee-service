package com.wtc.rest.internal;

import java.util.List;

import com.intershop.beehive.core.capi.app.AppContextUtil;
import com.intershop.component.application.capi.ApplicationBO;
import com.wtc.rest.capi.StudentResourceHandler;
import com.wtc.rest.capi.resourceobject.StudentRO;
import com.wtc.student.capi.StudentBO;
import com.wtc.student.capi.StudentBORepository;

public class StudentResourceHandlerImpl implements StudentResourceHandler
{
    private StudentBORepository getBORepository()
    {

        ApplicationBO currentApplicationBO = AppContextUtil.getCurrentAppContext().getVariable(ApplicationBO.CURRENT);
        return currentApplicationBO.getRepository(StudentBORepository.EXTENSION_ID);
    }

    @Override
    public List<StudentBO> getAll()
    {
        return getBORepository().getAllStudents();
    }

    @Override
    public StudentBO getStudent(String uuid)
    {
        return getBORepository().getStudentBOByUUID(uuid);
    }

    @Override
    public void addStudent(StudentRO studentRO)
    {
        getBORepository().createStudentBo(studentRO.getSid(), studentRO.getName(), studentRO.getCourse(),
                        studentRO.getAddress());

    }

    @Override
    public void deleteStudent(String uuid)
    {
        getBORepository().deleteStudentBO(uuid);

    }

    @Override
    public void updateStudent(StudentRO studentRO)
    {
        StudentBO studentBO = getBORepository().getStudentBOByUUID(studentRO.getUuid());
        studentBO.setSName(studentRO.getName());
        studentBO.setCourse(studentRO.getCourse());
        studentBO.setAddress(studentRO.getAddress());

        getBORepository().updateStudent(studentRO.getUuid(), studentBO.getSName(), studentBO.getCourse(),
                        studentBO.getAddress());
    }

    @Override
    public StudentBO getStudent(Integer sid)
    {

        return null;
    }

}
