package com.wtc.rest.internal;

import java.util.Objects;

import com.google.inject.Inject;
import com.intershop.beehive.core.capi.util.ObjectMapper;
import com.intershop.component.repository.capi.BusinessObjectRepositoryContextProvider;
import com.wtc.rest.capi.resourceobject.StudentRO;
import com.wtc.student.capi.StudentBO;

public class StudentBOToStudentROMapperImpl implements ObjectMapper<StudentBO, StudentRO>
{

    @Inject
    BusinessObjectRepositoryContextProvider businessObjectRepositoryContextProvider;

    @Override
    public StudentRO resolve(StudentBO studentBO)
    {

        Objects.requireNonNull(studentBO);

        return new StudentRO(studentBO.getSid(), studentBO.getSName(), studentBO.getCourse(), studentBO.getAddress(),
                        studentBO.getID());
    }

}
