<%@  page buffer="none" import="java.util.*,java.io.*,com.intershop.beehive.core.internal.template.*,com.intershop.beehive.core.internal.template.isml.*,com.intershop.beehive.core.capi.log.*,com.intershop.beehive.core.capi.resource.*,com.intershop.beehive.core.capi.util.UUIDMgr,com.intershop.beehive.core.capi.util.XMLHelper,com.intershop.beehive.foundation.util.*,com.intershop.beehive.core.internal.url.*,com.intershop.beehive.core.internal.resource.*,com.intershop.beehive.core.internal.wsrp.*,com.intershop.beehive.core.capi.pipeline.PipelineDictionary,com.intershop.beehive.core.capi.naming.NamingMgr,com.intershop.beehive.core.capi.pagecache.PageCacheMgr,com.intershop.beehive.core.capi.request.SessionMgr,com.intershop.beehive.core.internal.request.SessionMgrImpl,com.intershop.beehive.core.pipelet.PipelineConstants" extends="com.intershop.beehive.core.internal.template.AbstractTemplate" %><% 
boolean _boolean_result=false;
TemplateExecutionConfig context = getTemplateExecutionConfig();
createTemplatePageConfig(context.getServletRequest());
printHeader(out);
 %>
<% %><%@ page contentType="text/html;charset=utf-8" %><%setEncodingType("text/html"); %><div class="message"><% {String value = null;try{value=context.getFormattedValue(getObject("Dictionary:message"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {3}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %></div>
<Center>
<h2>Update Student Details</h2>
<div class="message"><% {String value = null;try{value=context.getFormattedValue(getObject("message"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {6}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %></div><% URLPipelineAction action1 = new URLPipelineAction(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null))),(new URLParameterSet().addURLParameter(context.getFormattedValue("UUID",null),context.getFormattedValue(getObject("StudentBO:ID"),null)))),null));String site1 = null;String serverGroup1 = null;String actionValue1 = context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null))),(new URLParameterSet().addURLParameter(context.getFormattedValue("UUID",null),context.getFormattedValue(getObject("StudentBO:ID"),null)))),null);if (site1 == null){  site1 = action1.getDomain();  if (site1 == null)  {      site1 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getDomainName();  }}if (serverGroup1 == null){  serverGroup1 = action1.getServerGroup();  if (serverGroup1 == null)  {      serverGroup1 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getServerGroup();  }}out.print("<form");out.print(" method=\"");out.print("post");out.print("\"");out.print(" action=\"");out.print(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null))),(new URLParameterSet().addURLParameter(context.getFormattedValue("UUID",null),context.getFormattedValue(getObject("StudentBO:ID"),null)))),null));out.print("\"");out.print(">");out.print(context.prepareWACSRFTag(actionValue1, site1, serverGroup1,true)); %><Table>
<tr>
<td>
<div class="required" >
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {12}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Sid:</label> 
</div> 
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {17}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {18}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Sid"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {19}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>" required="required" readonly="readonly"/> 
</div>
</td>
</tr>
<tr>
<td>
<div class="required">
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {26}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Name:</label>
</div>
</td>
<td>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {30}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {31}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:SName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {32}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>" />
</td>
</tr>
<tr>
<td>
<div>
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {38}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Course:</label> 
</div>
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {43}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {44}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Course"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {45}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"/>
</div>
</td>
</tr>
<tr>
<td>
<div>
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {52}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Address:</label> 
</div>
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {57}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {58}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Address"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {59}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"/>
</div>
</td>
</tr>
<tr rowspan="4">
<td>
<div span="4">
<button type="submit" align="right" value="Update" name="Sf_UpdateStudentDetails">Update</button>
</div> 
</td>
<td>
<div>
<button type="submit" align="right" value="Back" name="Sf_BackBtn">Back</button>
</div> 
</td>
</tr>
</Table>
<style>
.required:after { content:" *"; }
function validateForm() {
var x = document.forms["TrainingWebForm"]["Sid"].value;
var y = document.forms["TrainingWebForm"]["Name"].value;
if (x == "") {
alert("Sid must be filled out");
return false;
}else if(y == "") {
alert("Name must be filled out"); 
return false;
}
} 
.message{
opacity: 2;
margin-top: 25px;
font-size: 15px;
text-align: center;
}​ 
</style><% out.print("</form>"); %></Center>
<script>
$(".message").css("color","green");
</script><% printFooter(out); %>