<%@  page buffer="none" import="java.util.*,java.io.*,com.intershop.beehive.core.internal.template.*,com.intershop.beehive.core.internal.template.isml.*,com.intershop.beehive.core.capi.log.*,com.intershop.beehive.core.capi.resource.*,com.intershop.beehive.core.capi.util.UUIDMgr,com.intershop.beehive.core.capi.util.XMLHelper,com.intershop.beehive.foundation.util.*,com.intershop.beehive.core.internal.url.*,com.intershop.beehive.core.internal.resource.*,com.intershop.beehive.core.internal.wsrp.*,com.intershop.beehive.core.capi.pipeline.PipelineDictionary,com.intershop.beehive.core.capi.naming.NamingMgr,com.intershop.beehive.core.capi.pagecache.PageCacheMgr,com.intershop.beehive.core.capi.request.SessionMgr,com.intershop.beehive.core.internal.request.SessionMgrImpl,com.intershop.beehive.core.pipelet.PipelineConstants" extends="com.intershop.beehive.core.internal.template.AbstractTemplate" %><% 
boolean _boolean_result=false;
TemplateExecutionConfig context = getTemplateExecutionConfig();
createTemplatePageConfig(context.getServletRequest());
printHeader(out);
 %>
<% %><%@ page contentType="text/html;charset=utf-8" %><%setEncodingType("text/html"); %><Center>
<h2>Student form</h2>
<!-- Error handling --><% _boolean_result=false;try {_boolean_result=((Boolean)(getObject("TrainingWebForm:Invalid"))).booleanValue();} catch (Exception e) {Logger.debug(this,"Boolean expression in line {} could not be evaluated. False returned. Consider using the 'isDefined' ISML function.",6,e);}if (_boolean_result) { %><tr>
<td>
<table border="0" cellspacing="0" cellpadding="4" width="100%" class="error_box s">
<tbody>
<tr>
<td class="error_icon e top" width="1%"><% _boolean_result=false;try {_boolean_result=((Boolean)(getObject("TrainingWebForm:Sid:isError(\"error.required\")"))).booleanValue();} catch (Exception e) {Logger.debug(this,"Boolean expression in line {} could not be evaluated. False returned. Consider using the 'isDefined' ISML function.",13,e);}if (_boolean_result) { %><img src="<%=context.getFormattedValue(context.webRoot(),null)%>/images/error.gif" width="16" height="15" alt="" border="0"/> 
<div class="errrormessage">Sid should not be empty.</div><% } %></td>
</tr>
<tr> 
<td class="error_icon e top" width="1%"> 
<% _boolean_result=false;try {_boolean_result=((Boolean)(getObject("TrainingWebForm:Name:isError(\"error.required\")"))).booleanValue();} catch (Exception e) {Logger.debug(this,"Boolean expression in line {} could not be evaluated. False returned. Consider using the 'isDefined' ISML function.",21,e);}if (_boolean_result) { %><img src="<%=context.getFormattedValue(context.webRoot(),null)%>/images/error.gif" width="16" height="15" alt="" border="0"/> 
<div class="errrormessage">Name should not be empty.</div><% } %> 
</td>
</tr>
</tbody>
</table>
</td>
</tr><% } %><% URLPipelineAction action7 = new URLPipelineAction(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null)))),null));String site7 = null;String serverGroup7 = null;String actionValue7 = context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null)))),null);if (site7 == null){  site7 = action7.getDomain();  if (site7 == null)  {      site7 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getDomainName();  }}if (serverGroup7 == null){  serverGroup7 = action7.getServerGroup();  if (serverGroup7 == null)  {      serverGroup7 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getServerGroup();  }}out.print("<form");out.print(" method=\"");out.print("post");out.print("\"");out.print(" action=\"");out.print(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-DispatchRequests",null)))),null));out.print("\"");out.print(">");out.print(context.prepareWACSRFTag(actionValue7, site7, serverGroup7,true)); %><Table>
<tr>
<td>
<div class="required" >
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {38}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Sid:</label> 
</div> 
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {43}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {44}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Sid:Value"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {45}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>" /> 
</div>
</td>
</tr>
<tr>
<td>
<div class="required">
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {52}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Name:</label>
</div>
</td>
<td>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {56}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {57}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Name:Value"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {58}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>" />
</td>
</tr>
<tr>
<td>
<div>
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {64}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Course:</label> 
</div>
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {69}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {70}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Course:Value"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {71}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"/>
</div>
</td>
</tr>
<tr>
<td>
<div>
<label for="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {78}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>">Address:</label> 
</div>
</td>
<td>
<div>
<input type="text" id="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {83}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
name="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:QualifiedName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {84}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"
value="<% {String value = null;try{value=context.getFormattedValue(getObject("TrainingWebForm:Address:Value"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {85}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} %>"/>
</div>
</td>
</tr>
<tr>
<td>
<div class="right">
<a class="mylink" href="<%=context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewTraining-ViewList",null)))),null)%>">Back<a/>
</div> 
</td>
<td>
<div>
<button type="submit" align="right" value="Submit" name="Sf_SubmitStudentForm" style="float:right">Submit</button>
</div> 
</td>
</tr>
</Table><% out.print("</form>"); %></Center>
<style>
.required:after { content:" *"; }
function validateForm() {
var x = document.forms["TrainingWebForm"]["Sid"].value;
var y = document.forms["TrainingWebForm"]["Name"].value;
if (x == "") {
alert("Sid must be filled out");
return false;
}else if(y == "") {
alert("Name must be filled out"); 
return false;
}
}
.mylink {
display: block;
width: 60px;
height: 25px;
background: #E5E2E1;
padding: 1px;
text-align: center;
border-radius: 5px;
font-weight: bold;
} 
.right {
text-align: right;
float: right;
}
</style>
<script>
$(document).ready(function ()
{
$('title').text("Intershop 7 - Students");
});
$(".errrormessage").css("color","red"); 
</script><% printFooter(out); %>