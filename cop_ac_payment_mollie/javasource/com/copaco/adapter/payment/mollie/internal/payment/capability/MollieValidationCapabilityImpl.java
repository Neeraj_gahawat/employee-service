package javasource.com.copaco.adapter.payment.mollie.internal.payment.capability;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceTools;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.capability.Validate;
import com.intershop.api.service.payment.v1.result.ValidationResult;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

public class MollieValidationCapabilityImpl implements Validate
{
    private MollieServiceTools serviceTools;
    
    public MollieValidationCapabilityImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        serviceTools = new MollieServiceTools(serviceConfigurationBO);
        NamingMgr.injectMembers(serviceTools);
    }
    
    @SuppressWarnings("unchecked")
    public Result<ValidationResult> validate(PaymentContext context, Payable payable)
    {
        final ValidationResult validateResult = new ValidationResult();
        final Result<ValidationResult> result = new Result<>(validateResult);
        return null;
    }
}
