package com.copaco.adapter.payment.mollie.internal.modules;

import com.intershop.beehive.core.capi.naming.AbstractNamingModule;
import com.intershop.beehive.core.capi.url.URLComposition;

public class CopacoAcPaymentMollie extends AbstractNamingModule
{
    @Override
    protected void configure()
    {
        bind(URLComposition.class);
    }
}