package com.copaco.adapter.payment.mollie.internal.payment.capability;

import java.util.Map;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceConstants;
import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceTools;
import com.copaco.mollie.api.MollieClient;
import com.copaco.mollie.api.MollieException;
import com.copaco.mollie.api.objects.Payment;
import com.intershop.api.data.common.v1.Attribute;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.data.payment.v1.PaymentHistoryEntry;
import com.intershop.api.data.payment.v1.PaymentTransaction;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.capability.RedirectAfterCheckout;
import com.intershop.api.service.payment.v1.result.AuthorizationResult;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *
 */
public class MollieRedirectAfterCheckoutCapabilityImpl extends MollieRedirectingCapabilityImpl implements RedirectAfterCheckout
{
    public MollieRedirectAfterCheckoutCapabilityImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        super(serviceConfigurationBO);
    }
    
    // Mollie payment->paid implies authorization 
    // Capture is not possible in Mollie
    // true sets order->payment->Status to Captured -> payment can't be cancelled
    // false sets order->payment->Status to Authorized -> payment can be cancelled/refunded
    @Override
    public boolean isCapturingAuthorization()
    {
      return false;
    }
    
    @Override
    public Result<AuthorizationResult> onRedirectAfterCheckoutNotification(PaymentContext context, Payable payable,
                    Map<String, Object> parameters)
    {
        Logger.debug(this,"onRedirectAfterCheckoutNotification() called");
        AuthorizationResult authorizationResult = new AuthorizationResult();
        Result<AuthorizationResult> result = new Result<>(authorizationResult);
        MollieServiceTools tools = new MollieServiceTools(serviceConfigurationBO);
        MollieClient client = tools.getCallerService();
        PaymentHistoryEntry history = context.getPaymentTransaction().getLatestPaymentHistoryEntry(RedirectAfterCheckout.class.getSimpleName());
        Attribute<String> molliepaymentAttr = (Attribute<String>)history.getAttributes().get(MollieServiceConstants.PARAM_MOLLIE_PAYMENT_ID);
        String molliePaymentID=molliepaymentAttr.getValue();
        try
        {
            Payment payment = client.payments().get(molliePaymentID);
            if (payment.isPaid())
            {
                Logger.debug(this, "Payment received for Mollie payment id: {}", molliePaymentID);
                final PaymentTransaction paymentTransaction = context.getPaymentTransaction();
                String transactionID = paymentTransaction.getId();
                authorizationResult.setTransactionID(transactionID);
                authorizationResult.setTransactionAmount(context.getPayment().getPaymentTotalAmount());
                result.setState(Result.SUCCESS);
            }
            else if (payment.isCancelled() || payment.isExpired() || Payment.STATUS_FAILED.equals(payment.status))
            {
                Logger.debug(this, "Payment cancelled for Mollie payment id: {}", molliePaymentID);
                result.setState(Result.FAILURE);
                result.addError("PaymentCancelled", "Payment cancelled!");
            }
            else if (payment.isPending() || payment.isOpen())
            {
                Logger.debug(this, "Payment pending or open for Mollie payment id: {}", molliePaymentID);
                result.setState(Result.PENDING);
            }
        }
        catch(MollieException e)
        {
            Logger.error(this, "Payment failed for Mollie payment id: {}", molliePaymentID);
        }
        return result;
    }
    
}
