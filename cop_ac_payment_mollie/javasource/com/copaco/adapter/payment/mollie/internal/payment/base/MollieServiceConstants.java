package com.copaco.adapter.payment.mollie.internal.payment.base;

/**
 * @author mukul@eperium.com
 *
 */
public interface MollieServiceConstants
{
   // configuration for Mollie client
    public static final String PREF_API_KEY = "payment.mollie.param.apikey";
    public static final String PREF_TEST_URL = "payment.mollie.param.testURL";
    public static final String PREF_PROD_URL = "payment.mollie.param.prodURL";
    public static final String PREF_ENVIRONMENT = "payment.mollie.param.environment";
    public static final String API_VERSION = "payment.mollie.param.apiversion";
    public static final String STANDARD_SERVICE_ID = "MOLLIE_STANDARD";
    public static final String ACTIVATE_WEBHOOK="payment.mollie.param.activateWebHook";
    

    
    // TODO: Remove constants not needed    
    public static final String PREF_PASSCODE = "payment.mollie.param.password";
    public static final String PREF_SIGNATURE = "payment.mollie.param.signature";
    public static final String PREF_LOGOURL = "payment.mollie.param.logo";
    public static final String PREF_BRANDING_ID = "payment.mollie.param.branding";
    public static final String PREF_PAGE_STYLE = "payment.mollie.param.pagestyle";
    public static final String PREF_AUTOCAPTURE = "payment.mollie.param.autocapture";
    public static final String PREF_SEND_ADDRESS = "payment.mollie.param.send.address";
    public static final String PREF_OVERRIDE_ADDRESS = "payment.mollie.param.override.address";
    public static final String PREF_ORDER_VALUE_LABEL = "payment.mollie.param.ordervalue.label";
    
    // push notification
    
    public static final String NOTIFICATION_BANK_PENDING = "BankPending";
    public static final String NOTIFICATION_GIRO_SUCCESS = "GiroSuccess";
    public static final String NOTIFICATION_GIRO_CANCEL = "GiroCancel";
    
    public static final String PIPELINE_VIEW_MOLLIE_NOTIFICATION_RESPONSE = "ViewPaypalNotificationResponse-Start";
    
    // callback result 
    
    public static final String CALLBACK_PAYER_ACCOUNT = "Account";
    public static final String CALLBACK_PAYER_ID = "PayerID";
    public static final String CALLBACK_AUTH_TOKEN = "AuthToken";
    public static final String CALLBACK_REQUIRES_AFTER_ORDER_REDIRECT = "AfterOrderRedirectRequired";
    public static final String CALLBACK_NOTIFICATION_EMAIL = "NotificationEMail";
    
    public static final String HISTORY_CANCEL = "Cancel";
    public static final String HISTORY_REFUND = "Refund";
    public static final String HISTORY_CAPTURE = "Capture";
    public static final String HISTORY_AUTHORIZE = "Authorize";
    
    // parameters
    
    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_CALL = "call";
    public static final String PARAM_PAYMENT_ID = "PaymentID";
    public static final String PARAM_TRANSACTION_ID = "TransactionID";
    public static final String PARAM_MOLLIE_PAYMENT_ID = "MolliePaymentID";
    
    
    
    public static final String PARAM_SERVICE_TRANSACTION_ID = "ServiceTransactionID";

    // callback result 
    
    public static final String CALLBACK_ACCESS_CODE = "AccessCode";
    
    
    // errors and warnings
    
    public static final String ERROR_CODE_TECHNICAL = "TechnicalError";
    public static final String ERROR_CODE_NO_REDIRECT_CALLBACK = "NoRedirectCallback";
    public static final String ERROR_CODE_MOLLIE_SHIPTO_ADDRESS = "InvalidShipToAddress";
    public static final String ERROR_CODE_MISSING_PAYMENT_DATA = "MissingPaymentData";
    public static final String ERROR_CODE_MOLLIE_ERROR = "PayPalError";
    
    public static final String ERROR_MESSAGE_FASTCHECKOUT_ERROR_NO_REDIRECT_CALLBACK = "payment.mollie.fastcheckout.error.NoRedirectCallback";
    public static final String ERROR_MESSAGE_PAYMENT_MOLLIE_FASTCHECKOUT_ERROR_MISSING_PAYMENT_DATA = "payment.mollie.fastcheckout.error.MissingPaymentData";
    public static final String ERROR_MESSAGE_MOLLIE_SHIPTO_ADDRESS = "payment.mollie.error.InvalidShipToAddress";
    public static final String ERROR_MESSAGE_TECHNICAL = "payment.mollie.error.TechnicalError";
    public static final String ERROR_MESSAGE_PREFIX = "payment.mollie.error.";
    
    public static final String WARN_MESSAGE_PREFIX = "payment.mollie.warning.";
    
    public static final String LOG_REFUND_REASON = "RefundReason";
    public static final String LOG_CAPTURE_TRANSACTION_ID = "CaptureTransactionID";
    public static final String LOG_TRANSACTION_ID = "TransactionID";
    public static final String LOG_ERROR_MSG = "ErrorMsg";
    public static final String LOG_ERROR_MESSAGE = "ErrorMessage";
    public static final String LOG_ERROR_CODE = "ErrorCode";
    public static final String LOG_AMOUNT = "Amount";
    public static final String PIPELINE_VIEW_PAYPAL_NOTIFICATION_RESPONSE = "ViewMollieNotificationResponse-Start";
    
    


}
