package com.copaco.adapter.payment.mollie.internal.payment.capability;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceConstants;
import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceTools;
import com.copaco.mollie.api.MollieClient;
import com.copaco.mollie.api.objects.Payment;
import com.copaco.mollie.api.objects.PaymentRefund;
import com.intershop.api.data.common.v1.Attribute;
import com.intershop.api.data.common.v1.Money;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.data.payment.v1.PaymentHistoryEntry;
import com.intershop.api.data.payment.v1.PaymentTransaction;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.capability.RedirectAfterCheckout;
import com.intershop.api.service.payment.v1.capability.Refund;
import com.intershop.api.service.payment.v1.result.RefundResult;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *  for now this class will never be used by Mollie
 *  If they support Capture in future then this Capability can be used
 */
@SuppressWarnings("unchecked")
public class MollieRefundCapabilityImpl implements Refund
{
    private MollieServiceTools serviceTools;
    
    public MollieRefundCapabilityImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        serviceTools = new MollieServiceTools(serviceConfigurationBO);
        NamingMgr.injectMembers(serviceTools);
    }
    
    
    
  
    @Override
    public Result<RefundResult> refund(PaymentContext context, Payable payable, Money amount, String reason)
    {
        final RefundResult refundResult = new RefundResult();
        final Result<RefundResult> result = new Result<>(refundResult);
        final PaymentTransaction paymentTransaction = context.getPaymentTransaction();
        try
            {
                //Money amountCaptured = paymentTransaction.getAmountCaptured();
                final MollieClient service = serviceTools.getCallerService();
                PaymentHistoryEntry history = context.getPaymentTransaction().getLatestPaymentHistoryEntry(RedirectAfterCheckout.class.getSimpleName());
                Attribute<String> molliepaymentAttr = (Attribute<String>)history.getAttributes().get(MollieServiceConstants.PARAM_MOLLIE_PAYMENT_ID);
                String molliePaymentID=molliepaymentAttr.getValue();
                Payment payment = service.payments().get(molliePaymentID);
                refundResult.setTransactionAmount(amount);
                // TODO: amount needs to be converted to EURO 
                PaymentRefund refund=service.payments().refund(payment,amount.getValue(),reason);  
                // TODO: ideally refund status should be checked
                // and status should we updated later via webhook
                refundResult.setTransactionID(refund.id);
                result.setState(Result.SUCCESS);
             }
            catch (Exception exception)
            {
                refundResult.setTransactionID(paymentTransaction.getServiceTransactionId()); 
                result.setState(Result.FAILURE);
                result.addError(MollieServiceConstants.ERROR_CODE_TECHNICAL, MollieServiceConstants.ERROR_MESSAGE_TECHNICAL);
               // Logger.error(this,"Refund result failed {}",exception);
            }
 
        return result;
    }

}
