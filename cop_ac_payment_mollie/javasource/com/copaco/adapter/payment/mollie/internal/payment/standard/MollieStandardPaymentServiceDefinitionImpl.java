package com.copaco.adapter.payment.mollie.internal.payment.standard;

import com.copaco.adapter.payment.mollie.internal.payment.base.MolliePaymentServiceDefinitionImpl;
import com.intershop.component.service.capi.assignment.ServiceProvider;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *
 */
public class MollieStandardPaymentServiceDefinitionImpl extends MolliePaymentServiceDefinitionImpl
{

    @Override
    public ServiceProvider getServiceProvider(ServiceConfigurationBO serviceConfigurationBO)
    {
        return new PaymentServiceProvider(new MollieStandardPaymentServiceImpl(serviceConfigurationBO));
    }


}

