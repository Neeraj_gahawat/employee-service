package com.copaco.adapter.payment.mollie.internal.payment.base;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.intershop.api.data.address.v1.Address;
import com.intershop.api.data.common.v1.Money;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.PaymentService;
import com.intershop.api.service.payment.v1.result.ApplicabilityResult;
import com.intershop.beehive.core.capi.log.Logger;

/**
 * @author mukul@eperium.com
 *
 */
public abstract class MollieBasePaymentService implements PaymentService, MollieServiceConstants
{
    @Override
    public Collection<Class<?>> getPaymentParameterDescriptors(PaymentContext context)
    {
        return Collections.emptyList();

    }

    @Override
    public Result<ApplicabilityResult> getApplicability(Payable payable)
    {

        final ApplicabilityResult applicability = new ApplicabilityResult();
        final Result<ApplicabilityResult> result = new Result<>(applicability);
        result.setState(ApplicabilityResult.APPLICABLE);

        // first check if basket has an amount > 0
        final Money totalAmount = payable.getTotals().getGrandTotalGross();

        if (totalAmount == null || totalAmount.getValue() == null || totalAmount.getValue().signum() != 1)
        {
            result.setState(ApplicabilityResult.NOT_APPLICABLE);
            Logger.debug(this, "Mollie Payment not applicable as total amount is invalid/blank: {}", totalAmount);
        }

        final Address invoiceAdrerss = payable.getInvoiceToAddress();
        // line1, city, country code, (state if USD)
        if (null != invoiceAdrerss)
        {
            boolean line1 = StringUtils.isNotBlank(invoiceAdrerss.getLine1());
            boolean city = StringUtils.isNotBlank(invoiceAdrerss.getCity());
            boolean country = StringUtils.isNotBlank(invoiceAdrerss.getCountry());
            boolean mainDivision = (StringUtils.isNotBlank(invoiceAdrerss.getMainDivision()) || (!Locale.US.getCountry().equals(invoiceAdrerss.getCountry())));

            final boolean addressOK = (line1 && city && country && mainDivision);

            if (!addressOK)
            {
                result.setState(ApplicabilityResult.NOT_APPLICABLE);
                result.addError(ERROR_CODE_MOLLIE_SHIPTO_ADDRESS, ERROR_MESSAGE_MOLLIE_SHIPTO_ADDRESS);
                Logger.debug(this, "Mollie Payment not applicable as invoice address is invalid: {}",
                                payable.getInvoiceToAddress());
            }

        }

        return result;
    }

}
