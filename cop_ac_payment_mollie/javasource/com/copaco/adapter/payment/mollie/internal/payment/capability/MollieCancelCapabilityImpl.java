package com.copaco.adapter.payment.mollie.internal.payment.capability;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceConstants;
import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceTools;
import com.copaco.mollie.api.MollieClient;
import com.copaco.mollie.api.objects.Payment;
import com.intershop.api.data.common.v1.Attribute;
import com.intershop.api.data.payment.v1.EnumPaymentTransactionStatus;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.data.payment.v1.PaymentHistoryEntry;
import com.intershop.api.data.payment.v1.PaymentTransaction;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.capability.Cancel;
import com.intershop.api.service.payment.v1.capability.RedirectAfterCheckout;
import com.intershop.api.service.payment.v1.result.CancelResult;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *
 */
public class MollieCancelCapabilityImpl implements Cancel
{

    private MollieServiceTools serviceTools;
    
    public MollieCancelCapabilityImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        serviceTools = new MollieServiceTools(serviceConfigurationBO);
        NamingMgr.injectMembers(serviceTools);
    }
    

    @SuppressWarnings("unchecked")
    @Override
    public Result<CancelResult> cancel(PaymentContext context, Payable payable)
    {
        final CancelResult cancelResult = new CancelResult();
        final Result<CancelResult> result = new Result<>(cancelResult);

        final PaymentTransaction paymentTransaction = context.getPaymentTransaction();
        String transactionID = paymentTransaction.getServiceTransactionId();
        
        // if the transaction was not authorized, we don't need to call Mollie 
        if (canBeCancelled(context))
        {   try
            {
                final MollieClient service = serviceTools.getCallerService();
                PaymentHistoryEntry history = context.getPaymentTransaction().getLatestPaymentHistoryEntry(RedirectAfterCheckout.class.getSimpleName());
                Attribute<String> molliepaymentAttr = (Attribute<String>)history.getAttributes().get(MollieServiceConstants.PARAM_MOLLIE_PAYMENT_ID);
                String molliePaymentID=molliepaymentAttr.getValue();
                Payment payment = service.payments().get(molliePaymentID);
                service.payments().refund(payment,"Amount refunded as the order was cancelled by the user!");   
                result.setState(Result.SUCCESS);
                cancelResult.setTransactionID(transactionID);
            }
            catch (Exception exception)
            {
                //result.setErrorCode(payPalFailure.getErrorCode());
                result.setState(Result.FAILURE);
                result.addError(MollieServiceConstants.ERROR_CODE_TECHNICAL, MollieServiceConstants.ERROR_MESSAGE_TECHNICAL);
                Logger.error(this,"Amount refund failed {}",exception);
            }
        }

        return result;
    }

    
    @Override
    public boolean canBeCancelled(PaymentContext context)
    {
       if (!Cancel.super.canBeCancelled(context))
        {
            return false;
        }
        final PaymentTransaction paymentTransaction = context.getPaymentTransaction();
        final EnumPaymentTransactionStatus status = paymentTransaction.getStatus();
        // ideally only authorized payments can be cancelled/refunded
        return (status == EnumPaymentTransactionStatus.AUTHORIZED) && !paymentTransaction.isStatusPending();
    }
    
}
