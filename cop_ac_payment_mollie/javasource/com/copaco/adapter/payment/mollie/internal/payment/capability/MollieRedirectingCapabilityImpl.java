package com.copaco.adapter.payment.mollie.internal.payment.capability;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceConstants;
import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceTools;
import com.copaco.mollie.api.MollieClient;
import com.copaco.mollie.api.MollieException;
import com.copaco.mollie.api.objects.Payment;
import com.intershop.api.data.common.v1.Money;
import com.intershop.api.data.common.v1.changeable.StringAttribute;
import com.intershop.api.data.document.v1.DocumentInfo;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.data.payment.v1.PaymentTransaction;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.capability.EnumRedirectResultStatus;
import com.intershop.api.service.payment.v1.capability.RedirectingPayment;
import com.intershop.api.service.payment.v1.result.CallbackResult;
import com.intershop.api.service.payment.v1.result.RedirectAfterCheckoutCallbackResult;
import com.intershop.beehive.core.capi.currency.ExchangeRateProvider;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.request.Request;
import com.intershop.beehive.core.capi.url.Action;
import com.intershop.beehive.core.capi.url.Parameters;
import com.intershop.beehive.core.capi.url.URLComposition;
import com.intershop.beehive.foundation.quantity.CurrencyException;
import com.intershop.beehive.foundation.quantity.ExchangeRate;
import com.intershop.beehive.foundation.quantity.MoneyCalculator;
import com.intershop.beehive.foundation.util.LRUHashMap;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *
 */
public class MollieRedirectingCapabilityImpl implements RedirectingPayment
{
    private static final String EURO = "EUR";
    ServiceConfigurationBO serviceConfigurationBO;
    private static Map<String, String> parametersMap = Collections
                    .synchronizedMap(new LRUHashMap<String, String>(50, "MolliePaymentIDCache"));
    @Inject
    private ExchangeRateProvider exchangeRateProvider;
    private MoneyCalculator moneyCalculator;

    @Inject
    private URLComposition urlComposition;

    public MollieRedirectingCapabilityImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        this.serviceConfigurationBO = serviceConfigurationBO;
        moneyCalculator = MoneyCalculator.createHighPrecisionCalculator();
    }

    @Override
    public URI calculateRedirectURL(PaymentContext paymentContext, Payable payable, URI successURL, URI cancelURL,
                    URI failureURL)
    {

        String orderID = payable.getHeader().getDocumentInfo().getDocumentNo();
        MollieServiceTools tools = new MollieServiceTools(serviceConfigurationBO);
        MollieClient client = tools.getCallerService();
        final Money totalMinusLimitedTender = paymentContext.getPayment().getPaymentTotalAmount();
        String orderValueLabel = "Order ID: " + orderID;
        Payment payment;
        String mnemonic = totalMinusLimitedTender.getCurrency();
        com.intershop.beehive.foundation.quantity.Money sourceMoney = new com.intershop.beehive.foundation.quantity.Money(
                        totalMinusLimitedTender.getCurrency(), totalMinusLimitedTender.getValue());
        com.intershop.beehive.foundation.quantity.Money euro = null;
        if (EURO.equals(mnemonic))
        {
            euro = sourceMoney;
        }
        else
        {
            try
            {
                ExchangeRate rate = exchangeRateProvider.getExchangeRate(mnemonic, "EUR");
                euro = moneyCalculator.convert(rate, sourceMoney);
            }
            catch(CurrencyException e)
            {
                Logger.error(this, "Exception converting money to EURO : {}", e);
                return null;
            }
        }
        BigDecimal totalAmount = euro.getValue();
        try
        {
            // until the environment's server is on open-ip, the webhook shouldn't be activated
            String webHookURL = client.isActivateWebHook() ? composeURL(payable.getHeader().getDocumentInfo()) : null;
            payment = client.payments().create(totalAmount, orderValueLabel, successURL.toASCIIString(),
                            Request.getCurrent().getLocale().getLocaleID(),
                            Collections.singletonMap("order_id", orderID), webHookURL);
        }
        catch(MollieException e)
        {
            Logger.error(this, "Payment redirection failed! Exception: {}", e);
            // even though we direct to sucessURL, Mollie paymentID will be null
            // which will be correctly handled as FAILURE in callback() method
            return URI.create(successURL.toASCIIString());
        }
        String paymentId = payment.id;
        if (payment.isOpen())
        {
            Logger.debug(this, "Payment initialized for Mollie payment id: {}", paymentId);
        }
        parametersMap.put(orderID, paymentId);
        String paymentURL = payment.getPaymentUrl();
        Logger.debug(this, "Mollie paymentURL: {} for Basket id {}", paymentURL, orderID);
        return URI.create(paymentURL);
    }

    @Override
    public Result<? extends CallbackResult> onCallback(PaymentContext context, Payable payable,
                    EnumRedirectResultStatus status, Map<String, Object> parameters)
    {
        MollieServiceTools tools = new MollieServiceTools(serviceConfigurationBO);
        MollieClient client = tools.getCallerService();
        final RedirectAfterCheckoutCallbackResult callbackResult = new RedirectAfterCheckoutCallbackResult();
        final Result<RedirectAfterCheckoutCallbackResult> result = new Result<>(callbackResult);
        callbackResult.put(new StringAttribute(MollieServiceConstants.PARAM_TRANSACTION_ID,
                        context.getPaymentTransaction().getId()));
        String molliePaymentID = parametersMap.get(payable.getHeader().getDocumentInfo().getDocumentNo());
        // molliePaymentID->null implies incorrect BO configuration
        // or Mollie API server is down etc.
        if (StringUtils.isBlank(molliePaymentID))
        {
            Logger.error(this, "Payment failed for payment transaction id: "+context.getPaymentTransaction().getId());
            result.setState(Result.FAILURE);
            result.addError("PaymentFailure", "MOLLIE_STANDARD.RedirectingPayment.PaymentFailure");
        }
        else
        {
            callbackResult.put(new StringAttribute(MollieServiceConstants.PARAM_MOLLIE_PAYMENT_ID, molliePaymentID));
            try
            {
                Payment payment = client.payments().get(molliePaymentID);
                if (payment.isPaid() && EnumRedirectResultStatus.SUCCESS.equals(status))
                {
                    Logger.debug(this, "Payment received for Mollie payment id: {}", molliePaymentID);
                    final PaymentTransaction paymentTransaction = context.getPaymentTransaction();
                    String transactionID = paymentTransaction.getId();
                    callbackResult.setTransactionID(transactionID);
                    callbackResult.setTransactionAmount(context.getPayment().getPaymentTotalAmount());
                    result.setState(Result.SUCCESS);
                }
                else if (payment.isCancelled() || payment.isExpired() || Payment.STATUS_FAILED.equals(payment.status))
                {
                    Logger.debug(this, "Payment cancelled for Mollie payment id: {}", molliePaymentID);
                    // ISH automatically redirects back to payment methods page
                    // in case of result.setState(Result.FAILURE);
                    result.setState(Result.FAILURE);
                    result.addError("PaymentCancelledByUser",
                                    "MOLLIE_STANDARD.RedirectingPayment.PaymentCancelledByUser");
                }
                else if (payment.isPending() || payment.isOpen())
                {
                    // ISH automatically displays: "The order status remains pending until the payment transaction is
                    // complete."
                    Logger.debug(this, "Payment pending or open for Mollie payment id: {}", molliePaymentID);
                    result.setState(Result.PENDING);
                }

            }
            catch(MollieException e)
            {
                Logger.error(this, "Payment failed for Mollie payment id: {} {}", molliePaymentID, e);
            }
        }
        return result;
    }

    private String composeURL(DocumentInfo documentInfo)
    {
        final Parameters params = new Parameters();
        // this parameter is added to avoid NPE thrown by
        // ISH: NotificationStartNode pipelet line 187
        params.addParameter("pushNotification", "Mollie");
        return urlComposition.createSessionlessURL(null, null, null,
                        new Action(MollieServiceConstants.PIPELINE_VIEW_PAYPAL_NOTIFICATION_RESPONSE, null,
                                        documentInfo.getSiteName()),
                        params);
    }
}