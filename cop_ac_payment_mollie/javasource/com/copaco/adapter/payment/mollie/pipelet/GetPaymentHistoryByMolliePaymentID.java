package com.copaco.adapter.payment.mollie.pipelet;

import java.util.Collection;
import java.util.Iterator;

import com.copaco.adapter.payment.mollie.internal.payment.base.MollieServiceConstants;
import com.intershop.beehive.bts.internal.payment.PaymentHistoryPO;
import com.intershop.beehive.bts.internal.payment.PaymentHistoryPOFactory;
import com.intershop.beehive.core.capi.log.Logger;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.pipeline.Pipelet;
import com.intershop.beehive.core.capi.pipeline.PipeletExecutionException;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.orm.capi.common.CacheMode;
import com.intershop.beehive.orm.capi.common.ORMHelper;

/**
 * @author pandeymu
 *
 */
public class GetPaymentHistoryByMolliePaymentID extends Pipelet
{
    @SuppressWarnings("deprecation")
    private static final PaymentHistoryPOFactory phFactory=(PaymentHistoryPOFactory)NamingMgr.getInstance().lookupFactory(PaymentHistoryPO.class);
  

    @Override
    public int execute(PipelineDictionary aPipelineDictionary) throws PipeletExecutionException
    {
        String molliePaymentID=aPipelineDictionary.get("id");
        String condition1="EVENTID='RedirectAfterCheckout'";
        @SuppressWarnings("unchecked")
        Collection<PaymentHistoryPO> coll1=phFactory.getObjectsBySQLWhere(condition1,CacheMode.NO_CACHING);
        PaymentHistoryPO paymentHistoryPO = null;
        boolean found=false;
        for (Iterator<PaymentHistoryPO> iterator = coll1.iterator(); iterator.hasNext();)
        {
            paymentHistoryPO = iterator.next();
            if (molliePaymentID.equals(paymentHistoryPO.getString(MollieServiceConstants.PARAM_MOLLIE_PAYMENT_ID)))
            {
               found=true;
               //Logger.debug(this, "Found MolliePaymentID: {} for paymentHistory: {}",molliePaymentID, paymentHistoryPO.getUUID()); 
               ORMHelper.closeIterator(iterator);
               break;
            }
        }

        if (found)
        {
            aPipelineDictionary.put("PaymentHistoryID", paymentHistoryPO.getUUID());
            return PIPELET_NEXT;
        }
        else
        {
            return PIPELET_ERROR;
        }
    }

}
