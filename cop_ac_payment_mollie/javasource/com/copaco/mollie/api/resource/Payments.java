/**
 * Copyright (c) 2015, Impending
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @license     Berkeley Software Distribution License (BSD-License 2) http://www.opensource.org/licenses/bsd-license.php
 * @author      Freddie Tilley <freddie.tilley@impending.nl>
 * @copyright   Impending
 * @link        http://www.impending.nl
 */
package com.copaco.mollie.api.resource;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.copaco.mollie.api.MollieClient;
import com.copaco.mollie.api.MollieException;
import com.copaco.mollie.api.objects.Payment;
import com.copaco.mollie.api.objects.PaymentRefund;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.intershop.beehive.core.capi.log.Logger;

public class Payments extends BaseResource<Payment> {
    public Payments(MollieClient api) {
        super(api);
    }

    public Payment create(BigDecimal amount, String description,
            String redirectUrl,String localeID, Map<String, Object> meta, String webhookURL) throws MollieException
    {
        return create(amount, null, description, redirectUrl,webhookURL,localeID, meta);
    }

    public Payment create(BigDecimal amount, String method, String description,
            String redirectUrl,  String webhookURL, String localeID, Map<String, Object> meta) throws MollieException
    {
        LinkedHashMap<String, Object> payData = new LinkedHashMap<>();

        if (amount != null)
        {
            payData.put("amount", amount);
        }
        if (method != null)
        {
            payData.put("method", method);
        }
        if (description != null)
        {
            payData.put("description", description);
        }
        if (redirectUrl != null)
        {
            payData.put("redirectUrl", redirectUrl);
        }
        if (StringUtils.isNotBlank(webhookURL))
        {
            payData.put("webhookUrl", webhookURL); 
        }
        if (StringUtils.isNotBlank(localeID))
        {
            payData.put("locale", localeID);
        }

        if (meta != null)
        {
            payData.put("metadata", meta);
        }

        return this.create(payData);
    }

    /**
     * Refund a payment. The passed payment argument will be updated to include
     * the refund status.
     *
     * @param payment {@link Payment} the payment to refund
     * @return {@link PaymentRefund} a refund object or null
     * @throws MollieException if there was a problem creating the refund
     *
     * @see #refund(Payment payment, BigDecimal amount)
     */
    public PaymentRefund refund(Payment payment, String description) throws MollieException {
        return refund(payment, null,description);
    }

    /**
     * Refund a payment. This method can be used to partially refund a payment.
     * The passed payment argument will be updated to include the refund status.
     *
     * @param payment {@link Payment} the payment to refund
     * @param amount the amount of the payment to refund.
     * @param description The description of the refund you are creating (Max 140 char)
     * This will be shown to the consumer on their card or bank statement when possible
     * @return {@link PaymentRefund} a refund object or null
     * @throws MollieException if there was a problem creating the refund
     *
     * @see #refund(Payment payment)
     */
    public PaymentRefund refund(Payment payment, BigDecimal amount, String description) throws MollieException
    {
        String method = this.getResourceName() + "/" + payment.id + "/refunds";
        JsonObject result = null;
        String methodBody = null;

        if (amount != null || StringUtils.isNotBlank(description))
        {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            LinkedHashMap<String, Object> refundData = new LinkedHashMap<>();
            if (amount != null)
            {
               refundData.put("amount", amount);
            }
            if (StringUtils.isNotBlank(description))
            {
               refundData.put("description", description);
            }
            methodBody = gson.toJson(refundData);
            Logger.debug(this, "JSON posted: {}", methodBody);
        }

        result = this.performApiCall(REST_CREATE, method, methodBody);

        if (result != null) {
            Gson gson = new Gson();
            PaymentRefund refund = gson.fromJson(result, PaymentRefund.class);

            if (refund != null && refund.payment != null) {
                this.copyInto(refund.payment, payment);
            }

            return refund;
        }

        return null;
    }
}
