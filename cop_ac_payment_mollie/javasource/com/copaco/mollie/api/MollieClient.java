package com.copaco.mollie.api;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.copaco.mollie.api.objects.Payment;
import com.copaco.mollie.api.resource.Issuers;
import com.copaco.mollie.api.resource.Methods;
import com.copaco.mollie.api.resource.Payments;
import com.copaco.mollie.api.resource.PaymentsRefunds;
import com.intershop.beehive.core.capi.log.Logger;

public class MollieClient {
    /**
     * Version of our client.
     */
    public static final String CLIENT_VERSION = "1.0";

    /**
     * Endpoint of the remote API.
     */
    public static final String API_ENDPOINT = "https://api.mollie.nl";

    /**
     * Version of the remote API.
     */
    public String API_VERSION = "v1";

    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_DELETE = "DELETE";

    protected Methods _methods;
    protected Payments _payments;
    protected Issuers _issuers;

    protected String _apiEndpoint = API_ENDPOINT;
    protected String _apiKey;
    private boolean activateWebHook;

    public MollieClient() {
        this.initResources(this);
    }

    public MollieClient(Map<String, String> customConfigurationMap)
    {
        // loads payments etc. -> should be in all constructors
        this.initResources(this);
        String mode=customConfigurationMap.get("mode");
        String apiurl="test".equals(mode)?customConfigurationMap.get("testAPIURL"):customConfigurationMap.get("prodAPIURL");
        setApiEndpoint(apiurl);
        try
        {
            setApiKey(customConfigurationMap.get("apiKey"));
        }
        catch(MollieException e)
        {
            //Logger.error(this, "Error setting Mollie API key: {}", e.getMessage());
            throw new RuntimeException("Error initialising Mollie connector: "+e.getMessage());
        }
        API_VERSION=customConfigurationMap.get("apiVersion");
        if ("yes".equals(customConfigurationMap.get("activateWebHook")))
        {
            setActivateWebHook(true);
        }
        else
        {
            setActivateWebHook(false);
        }
        
    }

    protected final void initResources(MollieClient client) {
        _methods = new Methods(client);
        _payments = new Payments(client);
        _issuers = new Issuers(client);
    }

    public Methods methods() { return _methods; }
    public Payments payments() { return _payments; }
    public Issuers issuers() { return _issuers; }
    public PaymentsRefunds refundsWithPayment(Payment payment) {
        return new PaymentsRefunds(this, payment.id);
    }

    public String apiEndpoint() { return _apiEndpoint; }
    public final void setApiEndpoint(String endpoint) { _apiEndpoint = endpoint; }

    /**
     * Sets the api key
     *
     * @param apikey api key to set
     * @throws MollieException when the api key is invalid.
     */
    public final void setApiKey(String apikey) throws MollieException
    {
        if (!apikey.matches("^(?:live|test)_\\w+$")) {
            throw new MollieException("Invalid api key: \"" + apikey + "\". An API key must start with \"test_\" or \"live_\".");
        }
        _apiKey = apikey;
    }

    /**
     * Perform a http call with an empty body. This method is used by the
     * resource specific classes. Please use the payments() method to perform
     * operations on payments.
     *
     * @param method the http method to use
     * @param apiMethod the api method to call
     * @return result of the http call
     * @throws MollieException when the api key is not set or when there is a
     * problem communicating with the mollie server.
     * @see #performHttpCall(String method, String apiMethod, String httpBody)
     */
    public String performHttpCall(String method, String apiMethod) throws MollieException
    {
        return performHttpCall(method, apiMethod, null);
    }

    /**
     * Perform a http call. This method is used by the resource specific classes.
     * Please use the payments() method to perform operations on payments.
     *
     * @param method the http method to use
     * @param apiMethod the api method to call
     * @param httpBody the contents to send to the server.
     * @return result of the http call
     * @throws MollieException when the api key is not set or when there is a
     * problem communicating with the mollie server.
     * @see #performHttpCall(String method, String apiMethod)
     */
    public String performHttpCall(String method, String apiMethod, String httpBody) throws MollieException
    {
        URI uri = null;
        String result = null;

        if (_apiKey == null || _apiKey.trim().equals(""))
        {
            throw new MollieException("You have not set an api key. Please use setApiKey() to set the API key.");
        }

        try {
            URIBuilder ub = new URIBuilder(this._apiEndpoint + "/" + API_VERSION + "/" + apiMethod);
            uri = ub.build();
        } catch (URISyntaxException e) {
            Logger.error(this,"Exception: {}",e);
        }

        if (uri != null)
        {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            HttpRequestBase action = null;
            HttpResponse response = null;

            if (method.equals(HTTP_POST)) {
                action = new HttpPost(uri);
            } else if (method.equals(HTTP_DELETE)) {
                action = new HttpDelete(uri);
            } else {
                action = new HttpGet(uri);
            }

            if (httpBody != null && action instanceof HttpPost)
            {
                StringEntity entity = new StringEntity(httpBody, ContentType.APPLICATION_JSON);
                ((HttpPost)action).setEntity(entity);
            }

            action.setHeader("Authorization", "Bearer " + this._apiKey);
            action.setHeader("Accept", ContentType.APPLICATION_JSON.getMimeType());

            try {
                response = httpclient.execute(action);

                HttpEntity entity = response.getEntity();
                StringWriter sw = new StringWriter();

                IOUtils.copy(entity.getContent(), sw, "UTF-8");
                result = sw.toString();
                EntityUtils.consume(entity);
            } catch (Exception e) {
                throw new MollieException("Unable to communicate with Mollie");
            }

            try {
                httpclient.close();
            } catch (IOException e) {
                Logger.error(this,"Exception: {}",e);
                
            }
        }

        return result;
    }

    public boolean isActivateWebHook()
    {
        return activateWebHook;
    }

    public void setActivateWebHook(boolean activateWebHook)
    {
        this.activateWebHook = activateWebHook;
    }
}
