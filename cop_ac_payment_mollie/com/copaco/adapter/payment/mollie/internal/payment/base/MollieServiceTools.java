package com.copaco.adapter.payment.mollie.internal.payment.base;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.copaco.mollie.api.MollieClient;
import com.intershop.beehive.businessobject.capi.BusinessObject;
import com.intershop.beehive.configuration.capi.common.Configuration;
import com.intershop.component.service.capi.service.ConfigurationProvider;

/**
 * @author mukul@eperium.com
 *
 */
public class MollieServiceTools
{
//   @Inject private LocalizationProvider localizationProvider;
    
    private BusinessObject serviceConfigurationBO;
    private Configuration configuration;
    
    public MollieServiceTools(BusinessObject aSserviceConfigurationBO)
    {
        this.serviceConfigurationBO = aSserviceConfigurationBO;
        final ConfigurationProvider configProviderExtension = serviceConfigurationBO.getExtension(ConfigurationProvider.class);
        this.configuration = configProviderExtension.getConfiguration();
    }

    /**
     * returns the Mollie API access service      
     * @return the service to access the Mollie API
     */
    public synchronized MollieClient getCallerService()
    {
        final Map<String, String> customConfigurationMap = new HashMap<>();
        customConfigurationMap.put("mode", getConfiguration().getString(MollieServiceConstants.PREF_ENVIRONMENT));
        customConfigurationMap.put("apiKey", getConfiguration().getString(MollieServiceConstants.PREF_API_KEY));
        customConfigurationMap.put("testAPIURL", getConfiguration().getString(MollieServiceConstants.PREF_TEST_URL));
        customConfigurationMap.put("prodAPIURL", getConfiguration().getString(MollieServiceConstants.PREF_PROD_URL));
        customConfigurationMap.put("apiVersion", getConfiguration().getString(MollieServiceConstants.API_VERSION));
        customConfigurationMap.put("activateWebHook", getConfiguration().getString(MollieServiceConstants.ACTIVATE_WEBHOOK));
        MollieClient service = new MollieClient(customConfigurationMap);
        return service;
    }
    
    private Configuration getConfiguration()
    {
        return this.configuration;
    }
    
    /**
     * Gets the boolean flag with the given name from the service configuration.
     * @param preferenceKey mandatory name of the preference 
     * @param fallbackValue optional fallback value, if <code>null</code> <code>false</code> is used as fallback value.
     * @return retrieved setting of the flag or the fallback value if not set/found.
     */
    public boolean isPreferenceEnabled(final String preferenceKey, final Boolean fallbackValue)
    {
        Objects.requireNonNull(preferenceKey);
        final Boolean fallback = fallbackValue != null ? fallbackValue : Boolean.FALSE; 
        
        return getConfiguration().getBoolean(preferenceKey, fallback);
    }
    
    /**
     * Gets the configurable string with the given name from the service configuration.
     * @param preferenceKey mandatory name of the preference 
     * @return retrieved value of the preference or the <code>null</code> value if not set/found.
     */
    public String getPreferenceStringValue(final String preferenceKey)
    {
        Objects.requireNonNull(preferenceKey);
        return getConfiguration().getString(preferenceKey);
    }
}
