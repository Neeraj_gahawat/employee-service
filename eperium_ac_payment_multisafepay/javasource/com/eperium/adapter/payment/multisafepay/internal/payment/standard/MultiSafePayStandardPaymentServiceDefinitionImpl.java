package com.eperium.adapter.payment.multisafepay.internal.payment.standard;

import com.eperium.adapter.payment.multisafepay.internal.payment.base.MultiSafePayPaymentServiceDefinitionImpl;
import com.intershop.component.service.capi.assignment.ServiceProvider;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * @author mukul@eperium.com
 *
 */
public class MultiSafePayStandardPaymentServiceDefinitionImpl extends MultiSafePayPaymentServiceDefinitionImpl
{

    @Override
    public ServiceProvider getServiceProvider(ServiceConfigurationBO serviceConfigurationBO)
    {
        return new PaymentServiceProvider(new MultiSafePayStandardPaymentServiceImpl(serviceConfigurationBO));
    }


}

