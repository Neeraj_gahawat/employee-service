
package com.eperium.adapter.payment.multisafepay.internal.payment.base;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import com.intershop.api.data.address.v1.Address;
import com.intershop.api.data.common.v1.Money;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.PaymentService;
import com.intershop.api.service.payment.v1.result.ApplicabilityResult;
import com.intershop.beehive.core.capi.log.Logger;

/**
 * @author mukul@eperium.com
 *
 */
public abstract class MultiSafePayBasePaymentService implements PaymentService, MultiSafePayServiceConstants
{
    @Override
    public Collection<Class<?>> getPaymentParameterDescriptors(PaymentContext context)
    {
        return Collections.emptyList();
    }

    @Override
    public Result<ApplicabilityResult> getApplicability(Payable payable)
    {
        final ApplicabilityResult applicability = new ApplicabilityResult();
        final Result<ApplicabilityResult> result = new Result<>(applicability);
        result.setState(ApplicabilityResult.APPLICABLE);

        // first check if basket has an amount > 0
        final Money totalAmount = payable.getTotals().getGrandTotalGross();

        if (totalAmount == null || totalAmount.getValue() == null || totalAmount.getValue().signum() != 1)
        {
            result.setState(ApplicabilityResult.NOT_APPLICABLE);
            Logger.debug(this,"MultiSafePayment not applicable as total amount is invalid/blank: {}",totalAmount);
        }
        
        final Address invoiceAdrerss = payable.getInvoiceToAddress();
        // line1, city, country code, (state if USD)
        if (null != invoiceAdrerss)
        {
            final boolean addressOK = (
                            ((null != invoiceAdrerss.getLine1()) && (!"".equals(invoiceAdrerss.getLine1()))) &&
                            ((null != invoiceAdrerss.getCity()) && (!"".equals(invoiceAdrerss.getCity()))) &&
                            ((null != invoiceAdrerss.getCountry()) && (!"".equals(invoiceAdrerss.getCountry()))) &&
                            ( (!"".equals(invoiceAdrerss.getMainDivision()) || (!Locale.US.getCountry().equals(invoiceAdrerss.getCountry()))) )
                                            );
                            
            if(! addressOK)
            {
                result.setState(ApplicabilityResult.NOT_APPLICABLE);
                result.addError(ERROR_CODE_MULTISAFEPAY_SHIPTO_ADDRESS, ERROR_MESSAGE_MULTISAFEPAY_SHIPTO_ADDRESS);
                Logger.debug(this,"MultiSafePayment not applicable as invoice address is invalid: {}",payable.getInvoiceToAddress());
            }

        }

        return result;
    }
    
/*
    @Override
    public <T extends PaymentCapability> T getCapability(Class<T> capability)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getID()
    {
        // TODO Auto-generated method stub
        return null;
    }*/  
    

}
