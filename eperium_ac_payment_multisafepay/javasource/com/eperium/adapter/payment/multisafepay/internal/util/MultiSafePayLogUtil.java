/**
 * 
 */
package com.eperium.adapter.payment.multisafepay.internal.util;

import com.intershop.beehive.core.capi.log.Logger;

/**
 * @author mukul@eperium.com
 *
 */
public class MultiSafePayLogUtil
{

    /**
     * @param pFunction
     * @param pTime
     * @param pOutMessage
     * @param pInMessage
     */
    public static void logInfo(final String pFunction, final Long pTime, final String pOutMessage, final String pInMessage)
    {
        final StringBuilder tMsgBuilder = new StringBuilder();
        tMsgBuilder.append(pFunction);
        tMsgBuilder.append(" time[");
        tMsgBuilder.append(Long.toString(pTime));
        tMsgBuilder.append("]ms ");
        tMsgBuilder.append(pOutMessage);
        tMsgBuilder.append("|");
        tMsgBuilder.append(pInMessage);
        Logger.info(MultiSafePayLogUtil.class, tMsgBuilder.toString());
    }

    /**
     * Log IPN messages
     * 
     * @param pFunction
     * @param pInMessage
     */
    public static void logIPNInfo(final String pFunction, final String pInMessage)
    {
        final StringBuilder tMsgBuilder = new StringBuilder();
        tMsgBuilder.append(pFunction);
        tMsgBuilder.append("|");
        tMsgBuilder.append(pInMessage);
        Logger.info(MultiSafePayLogUtil.class, tMsgBuilder.toString());
    }
}
