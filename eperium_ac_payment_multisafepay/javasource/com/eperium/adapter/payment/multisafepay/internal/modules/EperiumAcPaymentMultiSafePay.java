package com.eperium.adapter.payment.multisafepay.internal.modules;

import com.intershop.beehive.core.capi.naming.AbstractNamingModule;
import com.intershop.beehive.core.capi.url.URLComposition;

/**
 * @author mukul@eperium.com
 *
 */
public class EperiumAcPaymentMultiSafePay extends AbstractNamingModule
{

    @Override
    protected void configure()
    {
        bind(URLComposition.class);
    }

}
