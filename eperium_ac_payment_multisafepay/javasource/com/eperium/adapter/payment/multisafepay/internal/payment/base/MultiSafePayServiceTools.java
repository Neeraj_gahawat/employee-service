/**
 * 
 */
package com.eperium.adapter.payment.multisafepay.internal.payment.base;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Objects;

import javax.inject.Inject;

import com.eperium.multisafepay.client.MultiSafepayClient;
import com.intershop.api.data.address.v1.Address;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.beehive.businessobject.capi.BusinessObject;
import com.intershop.beehive.configuration.capi.common.Configuration;
import com.intershop.beehive.core.capi.localization.LocalizationProvider;
import com.intershop.component.service.capi.service.ConfigurationProvider;


/**
 * @author mukul@eperium.com
 *
 */
public class MultiSafePayServiceTools
{
   @Inject private LocalizationProvider localizationProvider;
    
    private BusinessObject serviceConfigurationBO;
    private Configuration configuration;
    private DecimalFormat decimalFormat;
    
    public MultiSafePayServiceTools(BusinessObject aSserviceConfigurationBO)
    {
        this.serviceConfigurationBO = aSserviceConfigurationBO;
        final ConfigurationProvider configProviderExtension = serviceConfigurationBO.getExtension(ConfigurationProvider.class);
        this.configuration = configProviderExtension.getConfiguration();
        
        final DecimalFormatSymbols s = new DecimalFormatSymbols();
        s.setDecimalSeparator('.');
        this.decimalFormat = new DecimalFormat("0.00", s);
    }

    
   /* public synchronized PayPalAPIInterfaceServiceService getCallerService()
    {
     // TODO
    
        final Map<String, String> customConfigurationMap = new HashMap<>();
        customConfigurationMap.put("mode", getConfiguration().getString(PaypalServiceConstants.PREF_ENVIRONMENT));
        customConfigurationMap.put("acct1.UserName", getConfiguration().getString(PaypalServiceConstants.PREF_USERNAME));
        customConfigurationMap.put("acct1.Password", getConfiguration().getString(PaypalServiceConstants.PREF_PASSWORD));
        customConfigurationMap.put("acct1.Signature", getConfiguration().getString(PaypalServiceConstants.PREF_SIGNATURE));

        final PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(customConfigurationMap);
        
        return service;
    }*/
    
    /**
     * returns the Multisafepay API access service, sets the access parameters
     * "mode", 
     * "acct1.UserName", 
     * "acct1.Password", 
     * "acct1.Signature", 
     * by service configuration payment.multisafepay.param.* and the
     * "acct1.AppId
     * 
     * @return the service to access the Multisafepay API
     */
    public synchronized MultiSafepayClient getCallerService()
    {
        final MultiSafepayClient service = new MultiSafepayClient();
        service.init(true);
        return service;
    }
    
    private Configuration getConfiguration()
    {
        return this.configuration;
    }
    
    /**
     * Gets the boolean flag with the given name from the service configuration.
     * @param preferenceKey mandatory name of the preference 
     * @param fallbackValue optional fallback value, if <code>null</code> <code>false</code> is used as fallback value.
     * @return retrieved setting of the flag or the fallback value if not set/found.
     */
    public boolean isPreferenceEnabled(final String preferenceKey, final Boolean fallbackValue)
    {
        Objects.requireNonNull(preferenceKey);
        final Boolean fallback = fallbackValue != null ? fallbackValue : Boolean.FALSE; 
        
        return getConfiguration().getBoolean(preferenceKey, fallback);
    }
    
    /**
     * Gets the configurable string with the given name from the service configuration.
     * @param preferenceKey mandatory name of the preference 
     * @return retrieved value of the preference or the <code>null</code> value if not set/found.
     */
    public String getPreferenceStringValue(final String preferenceKey)
    {
        Objects.requireNonNull(preferenceKey);
        return getConfiguration().getString(preferenceKey);
    }
    
    /**
     * maps the payment details - tender (total amount) and locale information so far
     * 
     * @param paymentContext the payment context providing the total amount
     * @param payable representing the order to pay for
     * 
     * @return the list with the payment details to be provided to the PayPal API access service 
     */
    // mukul: I think not needed as this is like  mspClient.createOrder(order)?
/*    public List<PaymentDetailsType> setPaymentDetails(final PaymentContext paymentContext, final Payable payable)
    {

        final List<PaymentDetailsItemType> payPalItems = new ArrayList<PaymentDetailsItemType>();

        final List<PaymentDetailsType> payDetails = new ArrayList<PaymentDetailsType>();
        PaymentDetailsType paymentDetailsType = new PaymentDetailsType();

        final Money totalMinusLimitedTender = paymentContext.getPayment().getPaymentTotalAmount();
        
        LocaleInformation localeInformation = Request.getCurrent().getLocale();
        String orderValueLabel = localizationProvider.getText(LocalizationContext.create(localeInformation), PaypalServiceConstants.PREF_ORDER_VALUE_LABEL, (Object[])null);
        
        addItemToList(payPalItems, totalMinusLimitedTender, orderValueLabel, " ", 1, ItemCategoryType.PHYSICAL); 

        paymentDetailsType.setOrderTotal(convertMoney(totalMinusLimitedTender));
        paymentDetailsType.setPaymentDetailsItem(payPalItems);

        payDetails.add(paymentDetailsType);
        return payDetails;
    }
    */
    // mukul: not needed
   /* private void addItemToList(List<PaymentDetailsItemType> payPalItems, Money discountedPrice,
                    String displayName, String productSKU, int quantity, ItemCategoryType physical)
    {
        // hack somehow PayPal doesn't seems to support the transmission of lineItems with value zero
        // therefore theses line items aren't tansmitted
        if(!BigDecimal.ZERO.equals(discountedPrice.getValue()))
        {
            final PaymentDetailsItemType payPalItem = new PaymentDetailsItemType();
            payPalItem.setAmount(convertMoney(discountedPrice));
            payPalItem.setName(displayName);
            payPalItem.setNumber(productSKU);
            payPalItem.setQuantity(quantity);
             
            payPalItem.setItemCategory(physical);
            payPalItems.add(payPalItem);
        }
    }
   */ 
    /**
     * converts a INTERSHOP service API Money object to PayPal API's BasocAcountType
     * @see com.intershop.api.data.common.v1.Money, cartridge api_service,
     * @see urn.ebay.apis.CoreComponentTypes.BasicAmountType
     * 
     * @param amount the amount as Money object
     * @return the amount as BasocAcountType object
     */
    // mukul: not needed i think
  /*  public BasicAmountType convertMoney(final Money amount)
    {
        final BasicAmountType basicAmount = new BasicAmountType();
        basicAmount.setValue(this.decimalFormat.format(amount.getValue()));
        basicAmount.setCurrencyID(CurrencyCodeType.fromValue(amount.getCurrency()));
        return basicAmount;
    }*/
    
    /**
     * returns the shop to address to be offered to the consumer first, 
     * ot's the most frequently used one at the basket line items
     * @see com.intershop.api.data.address.v1.Address, cartridge api_service
     *
     * @param payable the Payable object representing the basket
     * 
     * @return the shop to address address
     */
    public Address getShippingAddress(Payable payable)
    {
        return payable.getBuckets().stream().filter(bucket -> bucket.getShipToAddress() != null).max(
                        (bucket1, bucket2) -> Double.compare(bucket1.getItems().stream().mapToDouble(item -> item.getLineItem().getQuantity().getValue().doubleValue()).sum(), 
                                                             bucket2.getItems().stream().mapToDouble(item -> item.getLineItem().getQuantity().getValue().doubleValue()).sum())
                        ).get().getShipToAddress();
    }

    /**
     * decides between error and warning in the authorization result and log it accordingly.
     * The acknowledgement status is considered a failure, if the acknowledge type returned by PayPal 
     * is not a success, partial success or a warning
     * 
     * @param result the result of type @see com.intershop.api.service.payment.v1.result.AuthorizationResult
     * @param errorList the errors
     * @param ackType the acknowledgement status by PayPal
     * 
     * @return the corrected result of type @see com.intershop.api.service.payment.v1.result.AuthorizationResult
     */
    // mukul: TODO for msp??? i think JSON response contains the response codes
/*    public Result<AuthorizationResult> handleErrorsAndAckType(final Result<AuthorizationResult> result, List<ErrorType> errorList, AckCodeType ackType)
    {
        String ackValue = ackType.getValue();
        if(!errorList.isEmpty())
        {
            if(ackValue.equalsIgnoreCase(AckCodeType.SUCCESSWITHWARNING.getValue()) || 
               ackValue.equalsIgnoreCase(AckCodeType.PARTIALSUCCESS.getValue()) || 
               ackValue.equalsIgnoreCase(AckCodeType.WARNING.getValue()))
            {
                for (ErrorType errorType : errorList)
                {
                    Logger.warn(this, "Warning during authorization from PayPal: {} {} ",errorType.getErrorCode(),errorType.getLongMessage());
                    result.addError(PaypalServiceConstants.ERROR_CODE_PAYPAL_ERROR, PaypalServiceConstants.WARN_MESSAGE_PREFIX + errorType.getErrorCode());
                }
            }
            else
            {
                for (ErrorType errorType : errorList)
                {
                    Logger.error(this, "Error from PayPal: {} {} ",errorType.getErrorCode(),errorType.getLongMessage());
                    result.addError(PaypalServiceConstants.ERROR_CODE_PAYPAL_ERROR, PaypalServiceConstants.ERROR_MESSAGE_PREFIX + errorType.getErrorCode());
                }
                result.setState(Result.FAILURE);
                return result;
            }
        }
        return result;
    }
*/    
    /**
     * converts a INTERSHOP service API address.@see com.intershop.api.data.address.v1.Addres into 
     * a PayPaö service API address @see urn.ebay.apis.eBLBaseComponents.AddressType
     * 
     * @param address the INTERSHOP service API address
     * @return a  PayPaö service API
     */
    // mukul: not needed i think
/*    public AddressType createPayPalAddress(final Address address)
    {
        AddressType paypalAddress =new AddressType();
        
        if (address != null) 
        {
            final Contact contact = address.getContact();
            paypalAddress.setName((contact.getFirstName() + " " + contact.getLastName()).trim());
            paypalAddress.setCityName(address.getCity());
            paypalAddress.setCountryName(address.getCountry());
            paypalAddress.setPostalCode(address.getPostalCode());
            paypalAddress.setStreet1(address.getLine1());
            paypalAddress.setStreet2(address.getLine2()+" "+address.getLine3());
    
            // assume validation for ISO code and state set if US is done elsewhere
            // (UI, (...ServiceImpl).getApplicability())
            String countryCode = address.getCountryCode();
            if ((countryCode != null) && (countryCode.length() == 2))
            {
                paypalAddress.setCountry(CountryCodeType.fromValue(countryCode));
            }
            
            final String stateOrProvince = address.getMainDivision();
            if ((stateOrProvince != null) && (stateOrProvince.length() > 0))
            {
                paypalAddress.setStateOrProvince(stateOrProvince);
            }
            Logger.debug(this, "createPayPalAddress() - country {}, state{}", countryCode, stateOrProvince);
        }
        return paypalAddress;
    }
*/    
    
    
}
