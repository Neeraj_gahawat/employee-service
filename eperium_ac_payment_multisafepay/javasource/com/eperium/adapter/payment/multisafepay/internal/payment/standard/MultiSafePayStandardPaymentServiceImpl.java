package com.eperium.adapter.payment.multisafepay.internal.payment.standard;



import java.util.Collection;
import java.util.Collections;

import com.eperium.adapter.payment.multisafepay.internal.payment.base.MultiSafePayBasePaymentService;
import com.intershop.api.data.payment.v1.PaymentContext;
import com.intershop.api.service.common.v1.Result;
import com.intershop.api.service.payment.v1.Payable;
import com.intershop.api.service.payment.v1.PaymentService;
import com.intershop.api.service.payment.v1.capability.PaymentCapability;
import com.intershop.api.service.payment.v1.result.ApplicabilityResult;
import com.intershop.component.service.capi.service.ServiceConfigurationBO;

/**
 * Implementation of the Mollie payment service connector for the Standard workflow.
 * 
 * @author mukul@eperium.com
 *
 */
public class MultiSafePayStandardPaymentServiceImpl extends MultiSafePayBasePaymentService implements PaymentService
{
    private static String SERVICE_ID = "MULTISAFEPAY_STANDARD";
    /*private RedirectAfterCheckout redirectAfterCheckout;
    private Cancel cancelCapability;
    private Refund refundCapability;
    private Validate validateCapability;*/

    
    public MultiSafePayStandardPaymentServiceImpl(ServiceConfigurationBO serviceConfigurationBO)
    {
        /*redirectAfterCheckout = new MollieRedirectAfterCheckoutCapabilityImpl(serviceConfigurationBO);
        NamingMgr.injectMembers(redirectAfterCheckout);
        cancelCapability = new MollieCancelCapabilityImpl(serviceConfigurationBO);
        NamingMgr.injectMembers(cancelCapability);
        refundCapability = new MollieRefundCapabilityImpl(serviceConfigurationBO);
        NamingMgr.injectMembers(refundCapability);
        validateCapability = new MollieValidationCapabilityImpl(serviceConfigurationBO);
        NamingMgr.injectMembers(validateCapability);*/
        
    }
    

    @Override
    public String getID()
    {
        return SERVICE_ID;
    }
    

    @SuppressWarnings("unchecked")
    @Override
    public <T extends PaymentCapability> T getCapability(Class<T> capability)
    {
       /*  if (capability.isAssignableFrom(RedirectAfterCheckout.class))
        {
            return (T)redirectAfterCheckout;
        }
        if (capability.isAssignableFrom(Cancel.class))
        {
            return (T)cancelCapability;
        }
        if (capability.isAssignableFrom(Refund.class))
        {
            return (T)refundCapability;
        }
        if (capability.isAssignableFrom(Validate.class))
        {
            return (T)validateCapability;
        }*/
        return null;
    }

    
    @Override
    public Result<ApplicabilityResult> getApplicability(Payable payable)
    {
        return super.getApplicability(payable);
    }

    @Override
    public Collection<Class<?>> getPaymentParameterDescriptors(PaymentContext context)
    {
        return Collections.emptyList();
    }
}
