/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.42
 * Generated at: 2019-11-13 06:25:06 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package ish.cartridges.sld_005fch_005fconsumer_005ftraining.default_;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.io.*;
import com.intershop.beehive.core.internal.template.*;
import com.intershop.beehive.core.internal.template.isml.*;
import com.intershop.beehive.core.capi.log.*;
import com.intershop.beehive.core.capi.resource.*;
import com.intershop.beehive.core.capi.util.UUIDMgr;
import com.intershop.beehive.core.capi.util.XMLHelper;
import com.intershop.beehive.foundation.util.*;
import com.intershop.beehive.core.internal.url.*;
import com.intershop.beehive.core.internal.resource.*;
import com.intershop.beehive.core.internal.wsrp.*;
import com.intershop.beehive.core.capi.pipeline.PipelineDictionary;
import com.intershop.beehive.core.capi.naming.NamingMgr;
import com.intershop.beehive.core.capi.pagecache.PageCacheMgr;
import com.intershop.beehive.core.capi.request.SessionMgr;
import com.intershop.beehive.core.internal.request.SessionMgrImpl;
import com.intershop.beehive.core.pipelet.PipelineConstants;

public final class StudentData_jsp extends com.intershop.beehive.core.internal.template.AbstractTemplate
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 0, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

 
boolean _boolean_result=false;
TemplateExecutionConfig context = getTemplateExecutionConfig();
createTemplatePageConfig(context.getServletRequest());
printHeader(out);
 
      out.write('\r');
      out.write('\n');
 
setEncodingType("text/html"); 
      out.write("<body>");
 _boolean_result=false;try {_boolean_result=((Boolean)(((((context.getFormattedValue(getObject("SelectedMenuItem"),null).equals(context.getFormattedValue("Student Data",null)))) ? Boolean.TRUE : Boolean.FALSE)))).booleanValue();} catch (Exception e) {Logger.debug(this,"Boolean expression in line {} could not be evaluated. False returned. Consider using the 'isDefined' ISML function.",4,e);}if (_boolean_result) { 
      out.write("<li class=\"selected\">");
 } 
      out.write("<div class=\"message\">");
 {String value = null;try{value=context.getFormattedValue(getObject("Dictionary:message"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {7}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</div>\n<center>\n<h2>Students data</h2>");
 URLPipelineAction action1 = new URLPipelineAction(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewStudent-FormDispatcher",null)))),null));String site1 = null;String serverGroup1 = null;String actionValue1 = context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewStudent-FormDispatcher",null)))),null);if (site1 == null){  site1 = action1.getDomain();  if (site1 == null)  {      site1 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getDomainName();  }}if (serverGroup1 == null){  serverGroup1 = action1.getServerGroup();  if (serverGroup1 == null)  {      serverGroup1 = com.intershop.beehive.core.capi.request.Request.getCurrent().getRequestSite().getServerGroup();  }}out.print("<form");out.print(" method=\"");out.print("post");out.print("\"");out.print(" action=\"");out.print(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewStudent-FormDispatcher",null)))),null));out.print("\"");out.print(">");out.print(context.prepareWACSRFTag(actionValue1, site1, serverGroup1,true)); 
      out.write("<table border=\"1\">\n<th>\n<th>Sr. No\n<th>Student ID</th>\n<th>Name</th>\n<th>Course</th>\n<th>Address</th> \n");
 while (loop("StudentList","StudentBO","c")) { 
      out.write("<tr><td><input type=\"checkbox\" name=\"DeleteMultiple\" value=\"");
      out.print(context.getFormattedValue(getObject("StudentBO:ID"),null));
      out.write("\"></td>\n<td colspan=\"1\">");
 {String value = null;try{value=context.getFormattedValue(getObject("c"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {21}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</td> \n<td><a href=\"");
      out.print(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewStudent-ShowStudentData",null))),(new URLParameterSet().addURLParameter(context.getFormattedValue("UUID",null),context.getFormattedValue(getObject("StudentBO:ID"),null)))),null));
      out.write('"');
      out.write('>');
 {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Sid"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {22}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</a></td> \n<td><a href=\"");
      out.print(context.getFormattedValue(url(true,(new URLPipelineAction(context.getFormattedValue("ViewStudent-ShowStudentData",null))),(new URLParameterSet().addURLParameter(context.getFormattedValue("UUID",null),context.getFormattedValue(getObject("StudentBO:ID"),null)))),null));
      out.write('"');
      out.write('>');
 {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:SName"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {23}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</a></td>\n<td>");
 {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Course"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {24}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</td>\n<td>");
 {String value = null;try{value=context.getFormattedValue(getObject("StudentBO:Address"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {25}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("</td>\n</tr> \n");
 } 
      out.write("<tr>\n<td colspan=\"4\"/>\n<input span=\"4\" class=\"txtbox\" type=\"text\" name=\"SearchBox\" value=\"");
 {String value = null;try{value=context.getFormattedValue(getObject("SearchBox"),null,null);}catch(Exception e){value=null;Logger.error(this,"ISPRINT has an invalid expression. Returning empty string. Line: {30}",e);}if (value==null) value="";value = encodeString(value);out.write(value);} 
      out.write("\" placeholder=\"Enter Sid or Name\"><i class=\"fa fa-search\"></i>\n<button class=\"button\" type=\"submit\" name=\"SearchBtn\" value=\"Search\"><span class=\"glyphicon glyphicon-search\"></span>Search</button>\n</input> \n<td><input type=\"submit\" class=\"button\" name=\"CreateBtn\" value=\"New\"> \n<td><input class=\"button\" type=\"submit\" name=\"DeleteBtn\" value=\"Delete\" onclick=\"return confirm('Are you sure you want to Delete ?');\">\n</table>");
 processOpenTag(response, pageContext, "flexpagingbar", new TagParameter[] {
new TagParameter("parametervalue",getObject("SearchType")),
new TagParameter("parametername1","IsSearchFired"),
new TagParameter("parametervalue1",getObject("IsSearchFired")),
new TagParameter("rowsperpage",getObject("RowsPerPage")),
new TagParameter("pageable","StudentList"),
new TagParameter("parametername","SearchType"),
new TagParameter("variablepagesize","true")}, 36); 
      out.write(' ');
      out.write('\n');
 out.print("</form>"); 
      out.write("</center>\n<style>\ntable {\nborder-collapse: collapse;\nborder-spacing: 2;\n}\nth,\ntd {\npadding: 7px 30px;\n}\nthead {\nbackground: #395870;\ncolor: #fff;\n}\ntbody tr:nth-child(even) {\nbackground: #f0f0f2;\n}\ntd {\nborder-bottom: 1px solid #cecfd5;\nborder-right: 1px solid #cecfd5;\n}\ntd:first-child {\nborder-left: 1px solid #cecfd5;\n}\n.button {\nbackground-color: #4CAF50;\nborder: none;\ncolor: white;\npadding: 15px 20px;\ntext-align: center;\ntext-decoration: none;\ndisplay: inline-block;\nfont-size: 16px;\nmargin: 4px 2px;\ncursor: pointer;\n}\n.txtbox {\nfont-size: 12pt;\nheight: 25px;\nwidth : 300px;\n}\n</style>\n</body>\n<script>\n$(\".message\").css(\"color\",\"green\");\n</script>");
 printFooter(out); 
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
