$(document).ready(function(){
    $("#ConfirmDelete").click(function () {
        if($('input[name="uuid"]:checked').length > 0){
            $("#confirmBox").show();
            $("#NotSelectedMsg").hide();
        }else{
            $("#NotSelectedMsg").show();
        }
        
        return false;
    });
    
    $("#messageBoxCancel").click(function () {
        $("#confirmBox").hide();
        return false;
    });
    
    $('input[name="uuid"]').click(function() {
        if($(this).is(':checked'))
            $("#NotSelectedMsg").hide();
    });
    
    $('#example').dataTable( {
        "pagingType": "full_numbers"
    } );
});
